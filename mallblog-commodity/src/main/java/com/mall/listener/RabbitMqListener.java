package com.mall.listener;

import com.mall.common.ResultCode;
import com.mall.es.config.EsInitWithMallblog;
import com.mall.exception.service.ServiceException;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
* @Description: 消息队列监听
* @Param:
* @return:
* @Author: @从零开始
* @Date: 2023-11-21
*/

@Component
public class RabbitMqListener {

    @Autowired
    private EsInitWithMallblog esInitWithMallblog;

    /**
     * 监听es消息队列
     * @param msg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = ResultCode.ES_PRODUCT),
            exchange = @Exchange(name = ResultCode.PRODUCT_EXCHANGE,type = ExchangeTypes.DIRECT),
            key = ResultCode.ES_ROUYINGKEY
    ))
    public void listenProductEs(String msg) {
        // 业务逻辑-更新es中的商品信息
        try {
            esInitWithMallblog.initEsProductInfo();
        } catch (IOException e) {
            throw new ServiceException("es索引库更新失败：" + e.getMessage());
        }
    }

    /**
     * 监听redis消息队列
     * @param msg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = ResultCode.REDIS_PRODUCT),
            exchange = @Exchange(name = ResultCode.PRODUCT_EXCHANGE,type = ExchangeTypes.DIRECT),
            key = ResultCode.REDIS_ROUYINGKEY
    ))
    public void listenProductRedis(String msg){
        // 业务逻辑
        System.out.println(msg);
    }
}

package com.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("commonserver")
public interface CommonFeign {

    /**
     * 通过键值获取redis中的数值
     * @param key
     * @return
     */
    @GetMapping("/redis/get/{key}")
    String getValue(@PathVariable("key") String key);


    /**
     * 向redis中存入数值
     * 封装多个参数的get请求时，每个参数都要加上 @RequestParam
     * @param key
     * @param value
     * @return
     */
    @GetMapping("/redis/put")
    String putValue(@RequestParam String key, @RequestParam String value);
}

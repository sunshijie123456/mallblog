package com.mall.pojo;

import lombok.Data;

import java.util.List;

@Data
public class FilterWithId {

    private List<Category> categories;
    private List<Brand> brands;
    private List<Type> types;

}

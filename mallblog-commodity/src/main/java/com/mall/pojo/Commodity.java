package com.mall.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@TableName("sh_commodity")
public class Commodity implements Serializable {
    private static final long serialVersionUID = 1L;

    // 表属性
    private Long id;  // 主键id
    private String name;   // 商品名称
    private BigDecimal price;   // 商品价格
    private String image;   // 商品图片
    // 商品的类别
    private Long category;
    // 商品的类型
    private Long type;
    // 商品当前的状态
    private Integer status;
    // 商品所属的品牌
    private Long brand;
    // 商品的描述信息
    private String description;

    // 商品的创建时间
    @TableField(fill = FieldFill.INSERT)    // 相关处理见  /common/
    private LocalDateTime createTime;

    // 商品信息的更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    // 商品的发布人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    // 商品的更新人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    // 是否是会员
    private Integer isMember;

}

package com.mall.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("sh_brand")
public class Brand implements Serializable {
    private static final long serialVersionUID = 1L;

    // 表属性
    // 主键 id
    private Long id;
    // 品牌名称
    private String name;
    // 类别 id
    private Long category;
    // 排序字段
    private Integer sort;
    // 状态
    private Integer status;

    // 类别的创建时间
    @TableField(fill = FieldFill.INSERT)    // 相关处理见  /common/MyMetaObjectHandler.class
    private LocalDateTime createTime;

    // 类别信息的更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    // 类别的发布人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    // 类别的更新人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}

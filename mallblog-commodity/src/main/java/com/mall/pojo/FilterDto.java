package com.mall.pojo;

import lombok.Data;

import java.util.List;

@Data
public class FilterDto {

    private String title;
    private Boolean isMore = false;
    private Boolean isShowMore = false;
    private List<Filter> children;

}

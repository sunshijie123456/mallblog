package com.mall.pojo;

import lombok.Data;

@Data
public class PageParameter {

    private String name;
    private Long categoryId;
    private Long typeId;
    private Long brandId;
    private Integer pageNum;
    private Integer pageSize;

}

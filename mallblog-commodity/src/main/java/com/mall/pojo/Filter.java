package com.mall.pojo;

import lombok.Data;

@Data
public class Filter {

    private Long id;
    private String label;
    private Boolean active = false;

}

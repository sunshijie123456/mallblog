package com.mall.es.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.es.dto.EsDto;
import com.mall.es.dto.SuggestionDto;
import com.mall.es.pojo.EsParameter;
import com.mall.es.service.EsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/es")
public class EsController {

    @Autowired
    private EsService esService;

    @PostMapping("/list")
    public ResultWeb<EsDto> GetlistByEs(@RequestBody EsParameter esParameter){

        EsDto esDto =  esService.getlist(esParameter);

        return  esDto.getTotal() != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品信息成功",esDto)
                :new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品信息失败");
    }

    @GetMapping("/suggestion")
    public ResultWeb<List<SuggestionDto>> getSuggestion(@RequestParam String key){

        List<SuggestionDto> list = esService.getSuggestion(key);

        return list != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功",list)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"查询失败");
    }
}

package com.mall.es.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.mall.dto.CommodityDto;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class CommodityEs extends CommodityDto {

    // 索引库中需要补全查询的字段
    private List<String> suggestion;

    public CommodityEs(CommodityDto commodityDto) {
        this.suggestion = new ArrayList<>();
        this.suggestion.add(commodityDto.getCategoryName());
        this.suggestion.add(commodityDto.getTypeName());

        // 品牌中会出现"/"分割的情况
        String brandName = commodityDto.getBrandName();
        if (brandName.contains("/")){
            String[] strings = brandName.split("/");
            this.suggestion.addAll(Arrays.asList(strings));
        }else{
            this.suggestion.add(brandName);
        }
    }
}

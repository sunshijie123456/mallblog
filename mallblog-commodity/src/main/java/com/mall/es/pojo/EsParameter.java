package com.mall.es.pojo;

import lombok.Data;

@Data
public class EsParameter {

    private String keyword;
    private String categoryName;
    private String typeName;
    private String brandName;
    private Integer pageNum;
    private Integer pageSize;

}

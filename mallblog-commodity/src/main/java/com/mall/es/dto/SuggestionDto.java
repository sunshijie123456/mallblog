package com.mall.es.dto;

import lombok.Data;

@Data
public class SuggestionDto {

    // 查询的补足信息
    private String value;
    // 前端页面渲染时，需要一个address属性，后端不需要赋值
    private String address;

}

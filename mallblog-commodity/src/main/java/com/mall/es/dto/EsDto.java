package com.mall.es.dto;

import com.mall.dto.CommodityDto;
import com.mall.pojo.Commodity;
import lombok.Data;

import java.util.List;

@Data
public class EsDto{
    private Integer total;
    private List<CommodityDto> data;

    public EsDto() {

    }

    public EsDto(Integer total, List<CommodityDto> commodityDtos) {
        this.total = total;
        this.data = commodityDtos;
    }
}

package com.mall.es.config;

import com.alibaba.fastjson.JSON;
import com.mall.dto.CommodityDto;
import com.mall.es.pojo.CommodityEs;
import com.mall.pojo.Brand;
import com.mall.pojo.Category;
import com.mall.pojo.Commodity;
import com.mall.pojo.Type;
import com.mall.service.BrandService;
import com.mall.service.CategoryService;
import com.mall.service.CommodityService;
import com.mall.service.TypeService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 项目启动，所有的bean加载到容器中，同步mysql数据库中的数据到es索引库中
 */
@Component
@Slf4j
public class EsInitWithMallblog implements ApplicationRunner {

    @Autowired
    private RestHighLevelClient client;
    @Autowired
    private CommodityService commodityService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private BrandService brandService;

    /**
     * 容器初始化成功，将数据库中的文件导入到es索引库中
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {

        initEsProductInfo();

    }

    public void initEsProductInfo() throws IOException {
        // 1.读取数据库中的数据
        List<Commodity> list = commodityService.list();
        List<CommodityEs> collect = list.stream().map((item) -> {
            CommodityDto dto = new CommodityDto();
            BeanUtils.copyProperties(item, dto);

            Category category = categoryService.getById(item.getCategory());
            dto.setCategoryName(category.getName());
            Type type = typeService.getById(item.getType());
            dto.setTypeName(type.getName());
            Brand brand = brandService.getById(item.getBrand());
            dto.setBrandName(brand.getName());
            CommodityEs es = new CommodityEs(dto);
            BeanUtils.copyProperties(dto, es);
            return es;
        }).collect(Collectors.toList());
        // 2.创建request对象，将数据同步到es中
        BulkRequest request = new BulkRequest();
        if (collect.size() > 0){
            // 如果数据库中不存在数据，会报错No request add
            // 3.编写dsl语句
            for (CommodityEs commodityEs : collect) {
                request.add(new IndexRequest("commodity")
                        .id(commodityEs.getId().toString())
                        .source(JSON.toJSONString(commodityEs), XContentType.JSON)
                );
            }
            // 4.发送请求
            client.bulk(request, RequestOptions.DEFAULT);
        }
    }


}

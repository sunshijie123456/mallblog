package com.mall.es.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.elasticsearch.client.RestHighLevelClient;

@Configuration
@EnableFeignClients
public class EsConfig {

    @Value("${es.adder}")
    private String esAdder;

    /**
     * 将 RestHighLevelClient 防止于spring容器中管理
     * RestHighLevelClient 这个类可能需要手动导入
     * @return
     */
    @Bean
    public RestHighLevelClient restHighLevelClient(){
        return new RestHighLevelClient(RestClient.builder(
//                HttpHost.create("http://182.92.193.205:9200")   // IP地址需要变动
                HttpHost.create(esAdder)   // IP地址需要变动
        ));
    }
}

package com.mall.es.service.impl;

import com.alibaba.fastjson.JSON;
import com.mall.common.ResultCode;
import com.mall.dto.CommodityDto;
import com.mall.es.dto.EsDto;
import com.mall.es.dto.SuggestionDto;
import com.mall.es.pojo.EsParameter;
import com.mall.es.service.EsService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
public class EsServiceImpl implements EsService {

    @Autowired
    private RestHighLevelClient client;

    /**
     * 分页查询商品的相关信息
     * @param esParameter
     * @return
     */
    @Override
    public EsDto getlist(EsParameter esParameter) {

        // 获取前端转递的参数
        String keyword = esParameter.getKeyword();
        String categoryName = esParameter.getCategoryName();
        String brandName = esParameter.getBrandName();
        String typeName = esParameter.getTypeName();

        Integer pageNum = esParameter.getPageNum();
        Integer pageSize = esParameter.getPageSize();

        // 创建request对象
        SearchRequest request = new SearchRequest(ResultCode.INDEXNAME);

        // 编写dsl语句
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (keyword == null || "".equals(keyword)){
            // 如果关键字为空的话，我们查询所有的信息
            boolQuery.must(QueryBuilders.matchAllQuery());
        }else{
            boolQuery.must(QueryBuilders.matchQuery("all",keyword));
        }
        
        // 商品的类别，类型，品牌作为过滤条件，我们不希望参与算分
        if (categoryName != null && !"".equals(categoryName)){
            boolQuery.filter(QueryBuilders.termQuery("categoryName",categoryName));
        }
        if (brandName != null && !"".equals(brandName)){
            boolQuery.filter(QueryBuilders.termQuery("brandName",brandName));
        }
        if (typeName != null && !"".equals(typeName)){
            boolQuery.filter(QueryBuilders.termQuery("typeName",typeName));
        }

        // 根据创建时间和是否会员进行相关算分
        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(
                boolQuery,
                new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
                      // 添加会员算法函数
                      new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                              QueryBuilders.termQuery("isMember",1),
                              ScoreFunctionBuilders.weightFactorFunction(50)
                      )
                }
        );
        // 设置算法方式求和
        functionScoreQueryBuilder.scoreMode(FunctionScoreQuery.ScoreMode.SUM);
        request.source().query(functionScoreQueryBuilder);

        // 分页查询
        request.source().from((pageNum - 1) * pageSize);
        request.source().size(pageSize);

        try {
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            return dealWithResult(response);
        } catch (IOException e) {
            // 后续整理出全局异常控制
           throw new RuntimeException(e);
        }
    }

    /**
     * 实现关键字的补足查询
     * @param key
     * @return
     */
    @Override
    public List<SuggestionDto> getSuggestion(String key) {

        try {
            SearchRequest request = new SearchRequest(ResultCode.INDEXNAME);

            request.source().suggest(new SuggestBuilder().addSuggestion(
                    key + "Suggestion",
                    SuggestBuilders.completionSuggestion("suggestion")
                    .prefix(key)
                    .skipDuplicates(true)
                    .size(10)
            ));
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            Suggest suggest = response.getSuggest();
            CompletionSuggestion suggestion = suggest.getSuggestion(key + "Suggestion");
            List<CompletionSuggestion.Entry.Option> options = suggestion.getOptions();
            List<SuggestionDto> list = new ArrayList<>();
            for (CompletionSuggestion.Entry.Option option : options) {
                SuggestionDto dto = new SuggestionDto();
                String name = option.getText().toString();
                dto.setValue(name);
                list.add(dto);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 将查询到JSON数据格式转变对象
     * @param response
     * @return
     */
    public EsDto dealWithResult(SearchResponse response){
        SearchHits hits = response.getHits();
        Integer total = (int) hits.getTotalHits().value;
        SearchHit[] hitsHits = hits.getHits();
        List<CommodityDto> list = new ArrayList<CommodityDto>();

        if (hitsHits != null) {
            for (SearchHit hitsHit : hitsHits) {
                // 获取数据
                String json = hitsHit.getSourceAsString();

                // 将JSON转换成对象
                CommodityDto CommodityDto = JSON.parseObject(json, CommodityDto.class);

                // 获取高亮之后的数值
                Map<String, HighlightField> fields = hitsHit.getHighlightFields();
                if (fields != null) {
                    HighlightField highlightField = fields.get("name");
                    if (highlightField != null) {
                        String name = highlightField.getFragments()[0].string();
                        CommodityDto.setName(name);
                    }
                }
                list.add(CommodityDto);
            }
        }
        return new EsDto(total, list);
    }
}

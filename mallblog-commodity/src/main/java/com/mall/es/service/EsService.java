package com.mall.es.service;

import com.mall.es.dto.EsDto;
import com.mall.es.dto.SuggestionDto;
import com.mall.es.pojo.EsParameter;

import java.util.List;

public interface EsService {

    // 分页差选商品信息
    EsDto getlist(EsParameter esParameter);

    // 实现补足查询
    List<SuggestionDto> getSuggestion(String key);
}

package com.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.dto.BrandDto;
import com.mall.dto.ResultPageDto;
import com.mall.pojo.Brand;

import java.util.List;

public interface BrandService extends IService<Brand> {
    // 分页查询品牌信息
    ResultPageDto<BrandDto> Getlist(String name, Integer pageNum, Integer pageSize, Long categoryId);

    // 根据id查询品牌信息
    List<Brand> Getinfo(Long id);
}

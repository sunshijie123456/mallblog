package com.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.dto.ResultPageDto;
import com.mall.dto.TypeDto;
import com.mall.pojo.Type;

import java.util.List;

public interface TypeService extends IService<Type> {
    // 根据类别id查询品牌信息
    List<Type> Getinfo(Long id);

    // 分页查询商品的类型信息
    ResultPageDto<TypeDto> Getlist(String name, Integer pageNum, Integer pageSize, Long categoryId);
}

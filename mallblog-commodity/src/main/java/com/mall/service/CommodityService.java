package com.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.common.QueryParameters;
import com.mall.dto.CommodityDto;
import com.mall.dto.EsCommodityDto;
import com.mall.pojo.Commodity;
import com.mall.pojo.FilterDto;
import com.mall.pojo.PageParameter;
import com.mall.dto.ResultPageDto;

import java.util.List;

public interface CommodityService extends IService<Commodity> {

    boolean saveCommodity(Commodity commodity);

    List<CommodityDto> getlist();

    ResultPageDto<CommodityDto> GetPage(PageParameter parameter);

    // 查询商品的筛选条件
    List<FilterDto> getFilter();

    // 查询商品的筛选条件---关联商品类别
    List<FilterDto> getFilterById(Long id);

    /**
     * 查询用户个人的商品信息
     * @param id
     * @return
     */
    ResultPageDto<Commodity> getPageCommodityById(Long id);
}

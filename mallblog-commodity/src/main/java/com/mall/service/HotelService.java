package com.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.pojo.Hotel;

public interface HotelService extends IService<Hotel> {
}

package com.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.pojo.Category;

import java.util.List;

public interface CategoryService extends IService<Category> {
    Page Getlist(String name, Integer pageNum, Integer pageSize);
}

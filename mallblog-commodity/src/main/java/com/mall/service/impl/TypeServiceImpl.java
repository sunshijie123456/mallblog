package com.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.dto.BrandDto;
import com.mall.dto.ResultPageDto;
import com.mall.dto.TypeDto;
import com.mall.mapper.TypeMapper;
import com.mall.pojo.Brand;
import com.mall.pojo.Category;
import com.mall.pojo.Type;
import com.mall.pojo.Type;
import com.mall.service.CategoryService;
import com.mall.service.TypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService {

    @Autowired
    private CategoryService categoryService;

    /**
     * 分页查询商品的类型信息
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ResultPageDto Getlist(String name, Integer pageNum, Integer pageSize, Long categoryId) {

        // 编写sql语句-利用关键字进行模糊查询
        LambdaQueryWrapper<Type> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(name != null,Type::getName,name);
        lambdaQueryWrapper.eq(categoryId != null,Type::getCategory,categoryId);

        // 分页查询
        Page<Type> page = new Page<>(pageNum,pageSize);
        Page<Type> TypeIPage = this.page(page, lambdaQueryWrapper);
        
        // 获取查询到的数据,并封装dto
        List<Type> records = TypeIPage.getRecords();
        List<TypeDto> collect = records.stream().map((item) -> {
            TypeDto TypeDto = new TypeDto();
            BeanUtils.copyProperties(item, TypeDto);

            // 查询类别名称
            Category category = categoryService.getById(item.getCategory());
            TypeDto.setCategoryName(category.getName());
            return TypeDto;
        }).collect(Collectors.toList());

        ResultPageDto<TypeDto> resultPageDto = new ResultPageDto<>();
        resultPageDto.setTotal((int)TypeIPage.getTotal());
        resultPageDto.setData(collect);

        return resultPageDto;
    }

    /**
     * 依据商品类别的菜单栏，获取下拉菜单栏的类型数据
     * @param id
     * @return
     */
    @Override
    public List<Type> Getinfo(Long id) {

        LambdaQueryWrapper<Type> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(id != null,Type::getCategory,id);
        List<Type> list = this.list(lambdaQueryWrapper);
        return list;
    }
}

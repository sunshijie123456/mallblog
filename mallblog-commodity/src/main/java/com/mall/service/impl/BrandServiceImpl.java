package com.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.dto.BrandDto;
import com.mall.mapper.BrandMapper;
import com.mall.pojo.Brand;
import com.mall.pojo.Category;
import com.mall.dto.ResultPageDto;
import com.mall.pojo.Type;
import com.mall.service.BrandService;
import com.mall.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService {

    @Autowired
    private CategoryService categoryService;

    /**
     * 分页查询商品的类型信息
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ResultPageDto Getlist(String name, Integer pageNum, Integer pageSize, Long categoryId) {

        // 编写sql语句-利用关键字进行模糊查询
        LambdaQueryWrapper<Brand> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(name != null,Brand::getName,name);
        lambdaQueryWrapper.eq(categoryId != null, Brand::getCategory,categoryId);

        // 分页查询
        Page<Brand> page = new Page<>(pageNum,pageSize);
        Page<Brand> brandIPage = this.page(page, lambdaQueryWrapper);

        // 获取查询到的数据,并封装dto
        List<Brand> records = brandIPage.getRecords();
        List<BrandDto> collect = records.stream().map((item) -> {
            BrandDto brandDto = new BrandDto();
            BeanUtils.copyProperties(item, brandDto);

            // 查询类别名称
            Category category = categoryService.getById(item.getCategory());
            brandDto.setCategoryName(category.getName());
            return brandDto;
        }).collect(Collectors.toList());

        ResultPageDto<BrandDto> resultPageDto = new ResultPageDto<>();
        resultPageDto.setTotal((int)brandIPage.getTotal());
        resultPageDto.setData(collect);

        return resultPageDto;
    }

    /**
     * 依据商品类别的菜单栏，获取下拉菜单栏的品牌数据
     * @param id
     * @return
     */
    @Override
    public List<Brand> Getinfo(Long id) {

        LambdaQueryWrapper<Brand> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(id != null,Brand::getCategory,id);
        List<Brand> list = this.list(lambdaQueryWrapper);
        return list;
    }
}

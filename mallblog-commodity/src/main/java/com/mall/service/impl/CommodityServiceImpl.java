package com.mall.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.common.QueryParameters;
import com.mall.common.ResultCode;
import com.mall.dto.CommodityDto;
import com.mall.dto.EsCommodityDto;
import com.mall.dto.ResultPageDto;
import com.mall.exception.service.ex.NullKeywodsException;
import com.mall.mapper.CommodityMapper;
import com.mall.pojo.*;
import com.mall.service.BrandService;
import com.mall.service.CategoryService;
import com.mall.service.CommodityService;
import com.mall.service.TypeService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements CommodityService {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private RestHighLevelClient client;
    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 保存商品信息到数据库
     * @param commodity
     * @return
     */
    @Override
    public boolean saveCommodity(Commodity commodity) {

        // 1.处理商品图片

        // 2.查询会员信息-调用用户模块的方法
       /* Integer isMember = clientFeign.getIsMember(commodity.getCreateUser());
        commodity.setIsMember(isMember == 1 ? 0 : 1);*/
        boolean flag = this.save(commodity);
        return flag;
    }

    /**
     * 获取商品的列表信息
     * @return
     */
    @Override
    public List<CommodityDto> getlist() {

        List<Commodity> list = this.list();
        // 查询类别、类型、品牌表获取数据
        List<CommodityDto> collect = list.stream().map((item) -> {
            CommodityDto commodityDto = new CommodityDto();

            // 将item中的商品信息拷贝到commodityDto中
            BeanUtils.copyProperties(item,commodityDto);
            // 查询类别表
            Category category = categoryService.getById(item.getCategory());
            commodityDto.setCategoryName(category.getName());
            // 查询类型表
            Type type = typeService.getById(item.getType());
            commodityDto.setTypeName(type.getName());
            // 查询品牌表
            Brand brand = brandService.getById(item.getBrand());
            commodityDto.setBrandName(brand.getName());

            return commodityDto;
        }).collect(Collectors.toList());

        return collect;
    }

    /**
     * 按条件分页查询商品信息
     * @param parameter
     * @return
     */
    @Override
    public ResultPageDto<CommodityDto> GetPage(PageParameter parameter) {

        // 获取前端转递的参数
        String name = parameter.getName();
        Long categoryId = parameter.getCategoryId();
        Long typeId = parameter.getTypeId();
        Long brandId = parameter.getBrandId();
        Integer pageNum = parameter.getPageNum();
        Integer pageSize = parameter.getPageSize();

        // 封装查询条件
        LambdaQueryWrapper<Commodity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name != null,Commodity::getName,name)
                .eq(categoryId != null,Commodity::getCategory,categoryId)
                .eq(typeId != null,Commodity::getType,typeId)
                .eq(brandId != null,Commodity::getBrand,brandId);
        Page<Commodity> page = new Page<>(pageNum,pageSize);
        Page<Commodity> commodityPage = this.page(page, queryWrapper);
        List<Commodity> records = commodityPage.getRecords();

        // 将查询到的数据封装成dto
        List<CommodityDto> collect = records.stream().map((item) -> {
            CommodityDto commodityDto = new CommodityDto();

            // 将item中的商品信息拷贝到commodityDto中
            BeanUtils.copyProperties(item, commodityDto);
            // 查询类别表
            Category category = categoryService.getById(item.getCategory());
            commodityDto.setCategoryName(category.getName());
            // 查询类型表
            Type type = typeService.getById(item.getType());
            commodityDto.setTypeName(type.getName());
            // 查询品牌表
            Brand brand = brandService.getById(item.getBrand());
            commodityDto.setBrandName(brand.getName());

            return commodityDto;
        }).collect(Collectors.toList());

        ResultPageDto<CommodityDto> pageDto = new ResultPageDto<>();
        pageDto.setTotal((int)commodityPage.getTotal());
        pageDto.setData(collect);

        return pageDto;
    }

    /**
     * 获取商品的过滤条件
     * @return
     */
    @Override
    public List<FilterDto> getFilter() {

        List<FilterDto> list = new ArrayList<>();
        // 首先查询redis中是否存在对应的键值
        if (redisTemplate.hasKey("category") != null && redisTemplate.hasKey("category")) {
            // 存在对应键值，直接获取value返回给前端
            String category = redisTemplate.opsForValue().get("category");
            FilterDto categoryDto = JSON.parseObject(category, FilterDto.class);

            Boolean typeflag = redisTemplate.hasKey("type");
            Boolean brandflag = redisTemplate.hasKey("brand");

            boolean flagT = typeflag != null && typeflag;
            boolean flagB = brandflag != null && brandflag;

            if (flagT && flagB){
                String brand = redisTemplate.opsForValue().get("brand");
                FilterDto brandDto = JSON.parseObject(brand, FilterDto.class);

                String type = redisTemplate.opsForValue().get("type");
                FilterDto typeDto = JSON.parseObject(type, FilterDto.class);

                list.add(categoryDto);
                list.add(brandDto);
                list.add(typeDto);
                return list;
            }
        }
        
        // 不存在，查询mysql获取数据
        List<Category> categorylist = categoryService.list();
        FilterDto categoryfilter = dealWithList(categorylist, "类别");
        list.add(categoryfilter);
        redisTemplate.opsForValue().set("category",JSON.toJSONString(categoryfilter),ResultCode.REDIS_DAY, TimeUnit.DAYS);

        List<Type> typelist = typeService.list();
        FilterDto typefilter = dealWithList(typelist, "类型");
        list.add(typefilter);
        redisTemplate.opsForValue().set("type",JSON.toJSONString(typefilter),ResultCode.REDIS_DAY, TimeUnit.DAYS);   // 将信息存储到redis中


        List<Brand> brandlist = brandService.list();
        FilterDto brandfilter = dealWithList(brandlist, "品牌");
        list.add(brandfilter);
        redisTemplate.opsForValue().set("brand",JSON.toJSONString(brandfilter),ResultCode.REDIS_DAY, TimeUnit.DAYS);

        return list;
    }

    /**
     * 查询商品的筛选条件---关联商品类别
     * @param id
     * @return
     */
    @Override
    public List<FilterDto> getFilterById(Long id) {

        List<FilterDto> list = new ArrayList<>();
        String keyName = "category";

        if (id != null){
            // id 不为空的话，拼接生成新的键值key
            keyName = keyName + id;
        }

        Boolean hasKey = redisTemplate.hasKey(keyName);
        if ( hasKey != null && hasKey){
            String json = redisTemplate.opsForValue().get(keyName);
            list = JSONObject.parseArray(json, FilterDto.class);
            return list;
        }

        List<Category> categoryList = categoryService.list();
        FilterDto categoryfilter = dealWithList(categoryList, "类别");
        list.add(categoryfilter);

        if (id == null){
            List<Type> typelist = typeService.list();
            FilterDto typefilter = dealWithList(typelist, "类型");
            list.add(typefilter);

            List<Brand> brandlist = brandService.list();
            FilterDto brandfilter = dealWithList(brandlist, "品牌");
            list.add(brandfilter);

            try {
                redisTemplate.opsForValue().set(keyName,JSON.toJSONString(list),ResultCode.REDIS_DAY,TimeUnit.DAYS);
            } catch (Exception exception) {

                exception.printStackTrace();
                log.error("数据写入redis异常: " + exception.getMessage());
            }
        }else {
            LambdaQueryWrapper<Type> typeQueryWrapper = new LambdaQueryWrapper<>();
            typeQueryWrapper.eq( Type::getCategory,id);
            List<Type> typeList = typeService.list(typeQueryWrapper);
            FilterDto typefilter = dealWithList(typeList, "类型");
            list.add(typefilter);

            LambdaQueryWrapper<Brand> brandQueryWrapper = new LambdaQueryWrapper<>();
            brandQueryWrapper.eq(Brand::getCategory,id);
            List<Brand> brandList = brandService.list(brandQueryWrapper);
            FilterDto brandfilter = dealWithList(brandList, "品牌");
            list.add(brandfilter);

            redisTemplate.opsForValue().set(keyName,JSON.toJSONString(list),ResultCode.REDIS_MINUTE,TimeUnit.MINUTES);
        }

        return list;
    }

    /**
     * 查询用户的商品信息
     * @param id
     * @return
     */
    @Override
    public ResultPageDto<Commodity> getPageCommodityById(Long id) {

        if (id == null){
            throw new NullKeywodsException("查询id不能为空");
        }

        LambdaQueryWrapper<Commodity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Commodity::getCreateUser,id);
        List<Commodity> list = this.list(queryWrapper);
        ResultPageDto<Commodity> pageDto = new ResultPageDto<>();
        pageDto.setData(list);
        pageDto.setTotal(list.size());

        return pageDto;
    }

    /**
     * 封装生成FilterDto方法
     * @param list
     * @param name
     * @param <T>
     * @return
     */
    public <T> FilterDto dealWithList(List<T> list,String name){

        List<Filter> collect = list.stream().map((item) -> {
            Filter filter = new Filter();
            Class<?> aClass = item.getClass();
            try {
                Method getName = aClass.getMethod("getName");
                String label = (String)getName.invoke(item);
                filter.setLabel(label);

                Method getId = aClass.getMethod("getId");
                Long id = (Long)getId.invoke(item);
                filter.setId(id);
            }catch (Exception e) {
                // 注意全局异常处理
                e.printStackTrace();
            }
            return filter;
        }).collect(Collectors.toList());
        FilterDto filter = new FilterDto();
        filter.setTitle(name);
        filter.setChildren(collect);
        return filter;
    }

    /**
     * 重载方法，增添类别id参数，方便前端高亮显示类别
     * @param list
     * @param name
     * @param categoryId
     * @param <T>
     * @return
     */
    public <T> FilterDto dealWithList(List<T> list,String name,Long categoryId){

        List<Filter> collect = list.stream().map((item) -> {
            Filter filter = new Filter();
            Class<?> aClass = item.getClass();
            try {
                Method getName = aClass.getMethod("getName");
                String label = (String)getName.invoke(item);
                filter.setLabel(label);

                Method getId = aClass.getMethod("getId");
                Long id = (Long)getId.invoke(item);
                filter.setId(id);

                if (id.equals(categoryId)){
                    filter.setActive(true);
                }
            }catch (Exception e) {
                // 注意全局异常处理
                e.printStackTrace();
            }
            return filter;
        }).collect(Collectors.toList());
        FilterDto filter = new FilterDto();
        filter.setTitle(name);
        filter.setChildren(collect);
        return filter;
    }

}

package com.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.mapper.CategoryMapper;
import com.mall.pojo.Category;
import com.mall.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    /**
     * 按条件对商品类别信息进行分页查询
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Page Getlist(String name, Integer pageNum, Integer pageSize) {

        // 编写sql语句-利用关键字进行模糊查询
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(name != null,Category::getName,name);

        // 分页查询
        Page<Category> page = new Page<>(pageNum,pageSize);
        Page<Category> categoryIPage = this.page(page, lambdaQueryWrapper);
        // 获取查询到的数据
        // 前端会计算接收的数据长度，并显示在页面上，无需转递数量
        return categoryIPage;
    }
}

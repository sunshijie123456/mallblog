package com.mall.dto;

import lombok.Data;

import java.util.List;

@Data
public class EsCommodityDto {

    private Integer total;
    private List<CommodityDto> data;

    public EsCommodityDto() {

    }

    public EsCommodityDto(Integer total, List<CommodityDto> data) {
        this.total = total;
        this.data = data;
    }
}

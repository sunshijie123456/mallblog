package com.mall.dto;

import com.mall.pojo.Brand;
import lombok.Data;

@Data
public class BrandDto extends Brand {
    private String categoryName;
}

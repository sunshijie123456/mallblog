package com.mall.dto;

import lombok.Data;

import java.util.List;

@Data
public class ResultPageDto<T>{

    private Integer total;
//    private T data;
    private List<T> data;
}

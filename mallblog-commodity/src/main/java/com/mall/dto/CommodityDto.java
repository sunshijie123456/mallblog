package com.mall.dto;

import com.mall.pojo.Commodity;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
public class CommodityDto extends Commodity {

    private String categoryName;
    private String typeName;
    private String brandName;
}

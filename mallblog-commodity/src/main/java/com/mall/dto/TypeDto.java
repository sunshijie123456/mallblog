package com.mall.dto;

import com.mall.pojo.Type;
import lombok.Data;

@Data
public class TypeDto extends Type {
    private String categoryName;
}

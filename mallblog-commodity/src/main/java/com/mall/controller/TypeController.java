package com.mall.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.dto.TypeDto;
import com.mall.pojo.Type;
import com.mall.dto.ResultPageDto;
import com.mall.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeController {

    @Autowired
    private TypeService typeService;

    /**
     * 保存商品类型信息
     * @param type
     * @return
     */
    @PostMapping("/save")
    public ResultWeb<String> SaveType(@RequestBody Type type){

        boolean flag = typeService.save(type);

        return flag ? new ResultWeb<String>(ResultCode.INSERT_SUCCESS,"保存商品类型成功") :
                new ResultWeb<String>(ResultCode.INSERT_ERROR,"保存商品类型失败");
    }

    /**
     * 按条件分页查询商品的类型信息
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    public ResultWeb<ResultPageDto> getlist(@RequestParam String name, Integer pageNum, Integer pageSize, Long categoryId){

        ResultPageDto<TypeDto> list = typeService.Getlist(name, pageNum, pageSize, categoryId);

        return list.getTotal() != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品类型信息成功",list) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品类型信息失败");
    }

    /**
     * 更新类型的信息或者状态
     * @param type
     * @return
     */
    @PutMapping("/update")
    public ResultWeb<String> updateCate(@RequestBody Type type){

        boolean flag = typeService.updateById(type);
        return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"更新类型信息成功") :
                new ResultWeb<>(ResultCode.UPDATE_ERROR,"更新类型信息失败");
    }

    /**
     * 删除类型的信息
     * @param type
     * @return
     */
    @DeleteMapping("/delete")
    public ResultWeb<String> deleteCate(@RequestBody Type type){
        boolean flag = typeService.removeById(type.getId());
        return flag ? new ResultWeb<>(ResultCode.DELETE_SUCCESS,"类型信息删除成功") :
                new ResultWeb<>(ResultCode.DELETE_ERROR,"类型信息删除失败");
    }

    /**
     * 获取所有的商品类型信息
     * @return
     */
    @GetMapping("/info")
    public ResultWeb<List<Type>> getinfo(){
        List<Type> typeList = typeService.list();
        return typeList != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品类型信息成功",typeList) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品类型信息失败");
    }

    /**
     * 根据商品类别的id 获取商品类型信息
     * @return
     */
    @GetMapping("/info/{id}")
    public ResultWeb<List<Type>> getinfoById(@PathVariable Long id){
        List<Type> TypeList = typeService.Getinfo(id);
        return TypeList != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品类型信息成功",TypeList) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品类型信息失败");
    }
}

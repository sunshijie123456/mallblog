package com.mall.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.pojo.Category;
import com.mall.dto.ResultPageDto;
import com.mall.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 保存商品类别信息
     * @param category
     * @return
     */
    @PostMapping("/save")
    public ResultWeb<String> SaveCategory(@RequestBody Category category){

        boolean flag = categoryService.save(category);

        return flag ? new ResultWeb<String>(ResultCode.INSERT_SUCCESS,"保存商品类别成功") :
                new ResultWeb<String>(ResultCode.INSERT_ERROR,"保存商品类别失败");
    }

    /**
     * 按条件分页查询商品的类别信息
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    public ResultWeb<ResultPageDto> getlist(@RequestParam String name,Integer pageNum,Integer pageSize){
        Page page = categoryService.Getlist(name, pageNum, pageSize);
        // 数据封装
        ResultPageDto<List<Category>> pageDto = new ResultPageDto<>();
        pageDto.setData(page.getRecords());
        pageDto.setTotal((int)page.getTotal());   // 查询的数据不会超过int的取值范围，不会发生精度丢失

        return page.getRecords() != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品类别信息成功",pageDto) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品类别信息失败");
    }

    /**
     * 更新类别的信息或者状态
     * @param category
     * @return
     */
    @PutMapping("/update")
    public ResultWeb<String> updateCate(@RequestBody Category category){

        boolean flag = categoryService.updateById(category);
        return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"更新类别信息成功") :
                new ResultWeb<>(ResultCode.UPDATE_ERROR,"更新类别信息失败");
    }

    /**
     * 删除类别的信息
     * @param category
     * @return
     */
    @DeleteMapping("/delete")
    public ResultWeb<String> deleteCate(@RequestBody Category category){
        boolean flag = categoryService.removeById(category.getId());
        return flag ? new ResultWeb<>(ResultCode.DELETE_SUCCESS,"类别信息删除成功") :
                new ResultWeb<>(ResultCode.DELETE_ERROR,"类别信息删除失败");
    }

    /**
     * 获取所有的商品类别信息
     * @return
     */
    @GetMapping("/info")
    public ResultWeb<List<Category>> getinfo(){
        List<Category> categoryList = categoryService.list();
        return categoryList != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品类别信息成功",categoryList) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品类别信息失败");
    }
}

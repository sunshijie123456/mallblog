package com.mall.controller;

import com.mall.common.QueryParameters;
import com.mall.common.ResultCode;
import com.mall.dto.CommodityDto;
import com.mall.dto.EsCommodityDto;
import com.mall.pojo.Commodity;
import com.mall.common.ResultWeb;
import com.mall.pojo.FilterDto;
import com.mall.pojo.PageParameter;
import com.mall.dto.ResultPageDto;
import com.mall.publisher.RabbitMqPublisher;
import com.mall.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class CommodityController {

    @Autowired
    private CommodityService commodityService;
    @Autowired
    private RabbitMqPublisher rabbitMqPublisher;

    /**
     * 发布商品信息
     * @param commodity
     * @return
     */
    @PostMapping("/save")
    public ResultWeb<String> SaveCommodity(@RequestBody Commodity commodity){

        boolean flag = commodityService.saveCommodity(commodity);

        if (flag){
            rabbitMqPublisher.sendMsgWithProductToEs("更新es中的商品信息");
        }

        return flag ? new ResultWeb<String>(ResultCode.INSERT_SUCCESS,"发布商品信息成功！") :
                new ResultWeb<String>(ResultCode.INSERT_ERROR,"发布商品信息失败！");
    }

    /**
     * 管理平台展示获取分页商品信息
     * @param parameter
     * @return
     */
    @PostMapping("/page")
    public ResultWeb<ResultPageDto<CommodityDto>> GetPage(@RequestBody PageParameter parameter){
        ResultPageDto<CommodityDto> pageDto = commodityService.GetPage(parameter);

        return pageDto.getTotal() != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"分页查询商品信息成功",pageDto)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"分页查询商品信息失败");
    }

    /**
     * 首页展示循环展示商品的信息
     * @return
     */
    @GetMapping("/list")
    public ResultWeb<List<CommodityDto>> Getlist(){

        List<CommodityDto> list = commodityService.getlist();

        if (list == null){
            return new ResultWeb<List<CommodityDto>>(ResultCode.QUERY_ERROR,"获取商品信息失败！");
        }
        return new ResultWeb<List<CommodityDto>>(ResultCode.QUERY_SUCCESS,"获取商品信息列表成功！",list);
    }

    /**
     * 更新商品信息或者状态
     * @param commodity
     * @return
     */
    @PutMapping("/update")
    public ResultWeb<String> UpdateCommodity(@RequestBody Commodity commodity){

        boolean flag = commodityService.updateById(commodity);

        return flag ? new ResultWeb<String>(ResultCode.UPDATE_SUCCESS,"更新商品信息成功！") :
                new ResultWeb<String>(ResultCode.UPDATE_ERROR,"更新商品信息失败！");
    }

    /**
     * 删除商品信息
     * @param commodity
     * @return
     */
    @DeleteMapping ("/delete")
    public ResultWeb<String> deleteCommodity(@RequestBody Commodity commodity){

        boolean flag = commodityService.removeById(commodity.getId());

        return flag ? new ResultWeb<String>(ResultCode.DELETE_SUCCESS,"删除商品信息成功！") :
                new ResultWeb<String>(ResultCode.DELETE_ERROR,"删除商品信息失败！");
    }

//    /**
//     * 查询商品的筛选条件
//     * @return
//     */
//    @GetMapping("/filter")
//    public ResultWeb<List<FilterDto>> GetFilter(){
//        List<FilterDto> list = commodityService.getFilter();
//        return list != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品过滤条件成功",list)
//                : new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品过滤条件失败");
//    }

    /**
     * 查询商品的筛选条件
     * @param id
     * @return
     */
    @GetMapping(value = {"/filter/{id}","/filter"})
    public ResultWeb<List<FilterDto>> GetFilterById(@PathVariable(required = false) Long id){
        List<FilterDto> list = commodityService.getFilterById(id);
        return list != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品过滤条件成功",list)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品过滤条件失败");
    }

    /**
     * 查询用户发布的商品信息
     * @param id
     * @return
     */
    @GetMapping("/person/{id}")
    public ResultWeb<ResultPageDto<Commodity>> getPageCommodityById(@PathVariable Long id){

        ResultPageDto<Commodity> result = commodityService.getPageCommodityById(id);

        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品信息成功",result);
    }
}

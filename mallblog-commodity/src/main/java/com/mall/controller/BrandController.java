package com.mall.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.dto.BrandDto;
import com.mall.dto.ResultPageDto;
import com.mall.pojo.Brand;
import com.mall.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService BrandService;

    /**
     * 保存商品品牌信息
     * @param brand
     * @return
     */
    @PostMapping("/save")
    public ResultWeb<String> SaveBrand(@RequestBody Brand brand){

        boolean flag = BrandService.save(brand);

        return flag ? new ResultWeb<String>(ResultCode.INSERT_SUCCESS,"保存商品品牌成功") :
                new ResultWeb<String>(ResultCode.INSERT_ERROR,"保存商品品牌失败");
    }

    /**
     * 按条件分页查询商品的品牌信息
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    public ResultWeb<ResultPageDto<BrandDto>> getlist(@RequestParam String name, Integer pageNum, Integer pageSize, Long categoryId){

        ResultPageDto<BrandDto> list = BrandService.Getlist(name, pageNum, pageSize,categoryId);

        return list.getTotal() != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品品牌信息成功",list) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品品牌信息失败");
    }

    /**
     * 更新品牌的信息或者状态
     * @param brand
     * @return
     */
    @PutMapping("/update")
    public ResultWeb<String> updateCate(@RequestBody Brand brand){

        boolean flag = BrandService.updateById(brand);
        return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"更新品牌信息成功") :
                new ResultWeb<>(ResultCode.UPDATE_ERROR,"更新品牌信息失败");
    }

    /**
     * 删除品牌的信息
     * @param brand
     * @return
     */
    @DeleteMapping("/delete")
    public ResultWeb<String> deleteCate(@RequestBody Brand brand){
        boolean flag = BrandService.removeById(brand.getId());
        return flag ? new ResultWeb<>(ResultCode.DELETE_SUCCESS,"品牌信息删除成功") :
                new ResultWeb<>(ResultCode.DELETE_ERROR,"品牌信息删除失败");
    }

    /**
     * 获取所有的商品品牌信息
     * @return
     */
    @GetMapping("/info")
    public ResultWeb<List<Brand>> getinfo(){
        List<Brand> BrandList = BrandService.list();
        return BrandList != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品品牌信息成功",BrandList) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品品牌信息失败");
    }

    /**
     * 根据商品类别的id 获取商品品牌信息
     * @return
     */
    @GetMapping("/info/{id}")
    public ResultWeb<List<Brand>> getinfo(@PathVariable Long id){
        List<Brand> BrandList = BrandService.Getinfo(id);
        return BrandList != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询商品品牌信息成功",BrandList) :
                new ResultWeb<>(ResultCode.QUERY_ERROR,"查询商品品牌信息失败");
    }
}

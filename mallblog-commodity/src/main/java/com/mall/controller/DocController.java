package com.mall.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.exception.service.ex.FileSizeLimitException;
import com.mall.utills.ValidateCodeUtills;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件的上传和下载
 */
@RestController
@RequestMapping("/doc")
public class DocController {

    // 文件存储在本地的路径
    private String path = "E:\\ideaProject\\Mallblog\\image\\";
//    private String path = "/usr/mallblog/image/";

    /**
     * 上传文件到指定的文件夹下
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public ResultWeb<String> upload(MultipartFile file){
        // 前端转递一个临时文件，当前代码实现将临死文件保存在指定的文件夹下

        // 1.获取转递的临时文件名
        String originalFilename = file.getOriginalFilename();
        // 2. 获取图片的后缀，从最后的 . 开始截取字符串
        String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
        // 3. 生成新的文件名，防止重复
        String filename = UUID.randomUUID().toString() + substring;
        // 4. 创建目录对象
        File dir = new File(path);
        if (!dir.exists()){
            // 如果文件夹不存在的话，创建
            dir.mkdirs();
        }

        long size = file.getSize();
        if (size > 1048576){
            throw new FileSizeLimitException("文件大小不能超过1M");
        }

        // 5.将文件保存在创建的文件夹下
        try{
            file.transferTo(new File(path + filename));
            System.out.println(path + filename);
        }catch (IOException e){
            e.printStackTrace();
        }

        // 上传成功后需要返回，文件的名称，后续存储商品信息时使用
        return new ResultWeb<>(ResultCode.INSERT_SUCCESS,"文件插入成功",filename);
    }

    /**
     * 加载文件到网页
     * @param name
     * @param response
     * @return
     */
    @GetMapping("/download")
    public ResultWeb<String> download(String name, HttpServletResponse response){

        try {
            // 通过输入流读取文件内容
            FileInputStream fileInputStream = new FileInputStream(new File(path + name));

            // 通过输出流将文件写回浏览器
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("image/jpeg");

            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }

            // 关闭资源
            outputStream.close();
            fileInputStream.close();
        }catch (Exception exception){
            exception.printStackTrace();
        }


        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"加载成功");
    }

    @GetMapping("/code")
    public void getCode(HttpServletRequest request, HttpServletResponse response){

        // 创建session
        HttpSession session = request.getSession();
        // 删除原有的session
        session.removeAttribute(ResultCode.VALIDATECODE);

        ValidateCodeUtills codeUtills = new ValidateCodeUtills();
        BufferedImage image = codeUtills.getRandCode();
        // 将生成的字符串存储到session中
        session.setAttribute(ResultCode.VALIDATECODE,codeUtills.getStrCode());
        System.out.println(session.getAttribute(ResultCode.VALIDATECODE));
        // 将BufferedImage转换成byte
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(image,"jpeg",out);
            byte[] bytes = out.toByteArray();
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("image/jpeg");

            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

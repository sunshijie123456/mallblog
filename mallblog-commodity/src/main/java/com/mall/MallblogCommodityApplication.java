package com.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class MallblogCommodityApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallblogCommodityApplication.class, args);
    }

}

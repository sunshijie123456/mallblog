package com.mall;

import com.alibaba.fastjson.JSON;
import com.mall.pojo.Category;
import com.mall.pojo.Hotel;
import com.mall.publisher.RabbitMqPublisher;
import com.mall.service.CategoryService;
import com.mall.service.HotelService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.util.List;

@SpringBootTest(classes = MallblogCommodityApplication.class)
class MallblogCommodityApplicationTests {

    @Autowired
    private HotelService hotelService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RabbitMqPublisher rabbitMqPublisher;

    @Test
    void contextLoads() {
    }

    /**
     * 测试数据库 mysql 连通
     */
    @Test
    void TestConnectionMysql(){
        List<Hotel> list = hotelService.list();
        for (Hotel hotel : list) {
            System.out.println(hotel);
        }
        // 连接成功
    }

    /**
     * 测试 redis 连通
     */
    @Test
    void TestConnectionRedis(){
        String s = redisTemplate.opsForValue().get("num");
        System.out.println(s);
        // 连接成功
    }

    /**
     * 测试es的联通
     */
    @Test
    void TestConnectionES() throws IOException {
        // 构建request对象
        SearchRequest request = new SearchRequest("commodity");
        // 编写dsl语句
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        // 发送请求
        request.source().query(boolQueryBuilder);
        SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);

        // 对结果进行解析
        SearchHits hits = response.getHits();
        SearchHit[] hitsHits = hits.getHits();
        for (SearchHit hitsHit : hitsHits) {
            String sourceAsString = hitsHit.getSourceAsString();
            System.out.println(sourceAsString);
        }
    }

//    @Test
//    void testValue(){
//        String ssj = commonFeign.getValue("ssj");
//        System.out.println(ssj);
//    }

    @Test
    void TestRedisSaveObject(){
        List<Category> list = categoryService.list();

        redisTemplate.opsForValue().set("category", JSON.toJSONString(list));
    }

    @Test
    void TestRabbitMqSendMsg(){
//        rabbitMqPublisher.sendMsgWithProductToEs("请更新es中的商品信息");
        rabbitMqPublisher.sendMsgWithProductToRedis("请更新redis中的商品信息");
    }

}

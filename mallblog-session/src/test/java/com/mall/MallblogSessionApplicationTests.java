package com.mall;

import com.mall.utills.MyUUID;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {MallblogSessionApplication.class})
class MallblogSessionApplicationTests {

    @Test
    void contextLoads() {


    }

    @Test
    void testMyUUID(){
        System.out.println(MyUUID.getGuid());
    }

}

package com.mall.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel("消息列表dto")
@Data
public class MessageList {

    @ApiModelProperty("消息发送人id")
    private Long userId;

    @ApiModelProperty("消息发送人昵称")
    private String userName;

    @ApiModelProperty("消息发送人头像")
    private String photo;

    @ApiModelProperty("未读消息数量")
    private Integer number;

    @ApiModelProperty("未读消息")
    private List<MsgDetail> msg;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发送时间")
    private LocalDateTime time;

}

package com.mall.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "前端用户会话分页接收信息")
public class MessageDto<T> {

    @ApiModelProperty("记录总数")
    private Integer total;
    @ApiModelProperty("记录")
    private List<T> data;
}

package com.mall.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mall.pojo.Message;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(value = "后端返回分页查询会话信息")
public class MessagePage {

    @ApiModelProperty(value = "会话人1")
    private Long talkerA;
    @ApiModelProperty(value = "会话人2")
    private Long talkerB;
    @ApiModelProperty(value = "会话人1名称")
    private String talkerAName;
    @ApiModelProperty(value = "会话人2名称")
    private String talkerBName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "消息最新时间")
    private LocalDateTime newTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "消息最迟时间")
    private LocalDateTime oldTime;

    @ApiModelProperty(value = "消息列表")
    private List<Message> messages;
    @ApiModelProperty(value = "消息数量")
    private Long number;

}

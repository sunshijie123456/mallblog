package com.mall.dto;

import io.swagger.models.auth.In;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MsgDetail {

    private Integer type;
    private LocalDateTime time;
    private Integer msgType;
    private String message;
    private String photo;
}

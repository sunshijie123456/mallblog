package com.mall.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.dto.MessageList;
import com.mall.pojo.Message;
import com.mall.pojo.SysMessageParam;
import com.mall.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("session")
@Api(tags = "用户会话消息接口")
public class MessageController {

    @Autowired
    private MessageService messageService;

    /**
     * 会话界面左侧的消息列表查询
     * @param id
     * @return
     */
    @PostMapping("/list/{id}")
    @ApiOperation(value = "查询消息列表")
    public ResultWeb<List<MessageList>> getMsgList(@PathVariable Long id){

        List<MessageList> list = messageService.getMsgList(id);

        return list != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询消息列表成功",list)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"查询消息列表成功");
    }

    /**
     * 已读用户消息
     * @param userId
     * @param senderId
     * @return
     */
    @GetMapping("/readMsg/{userId}/{senderId}")
    public ResultWeb<String> readSenderMsg(@PathVariable Long userId, @PathVariable Long senderId){

        Boolean flag = messageService.readSenderMsg(userId,senderId);

        return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"消息已读成功")
                : new ResultWeb<>(ResultCode.UPDATE_ERROR,"消息已读失败");
    }

}

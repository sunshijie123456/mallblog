package com.mall.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.dto.MessageDto;
import com.mall.pojo.SysMessage;
import com.mall.pojo.SysMsgParemeter;
import com.mall.service.SysMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/** 
* @Description: 系统信息接收控制类
* @Param: 
* @return:
* @Author: @从零开始
* @Date: 2023-10-18
*/

@RestController
@RequestMapping("sys")
@Api(tags = "系统信息接口类")
public class SysMessageController {

    @Autowired
    private SysMessageService sysMessageService;

    /**
     * 发送消息给指定用户
     * @param sysMessage
     * @return
     */
    @PostMapping("/sendMsg")
    @ApiOperation(value = "发送消息给指定用户接口")
    public ResultWeb<String> sendSysMessage(@RequestBody SysMessage sysMessage){

        Boolean flag = sysMessageService.sendMsg(sysMessage);

        return flag ? new ResultWeb<>(1,"发送消息成功") : new ResultWeb<>(0,"发送消息失败");
    }

    /**
     * 获取用户未读消息
     * 前端页面最多展示五条消息
     * @return
     */
    @GetMapping("/page/{id}")
    @ApiOperation(value = "获取用户未读消息接口")
    public ResultWeb<List<SysMessage>> getMsgList(@PathVariable Long id){

        List<SysMessage> list = sysMessageService.getMsgList(id);

        return list != null ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询消息成功",list)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"查询消息失败");
    }

    /**
     * 用户消息已读
     * @param ids
     * @return
     */
    @GetMapping("/update")
    @ApiOperation(value = "消息已读接口")
    @Transactional
    public ResultWeb<String> updateStatus(@RequestParam(value = "ids") String ids){

        Boolean flag = sysMessageService.updateStatus(ids);
        return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"消息全部已读成功")
                : new ResultWeb<>(ResultCode.UPDATE_ERROR,"消息已读失败");
    }

    /**
     *分页查询系统消息
     * @param sysMsgParemeter
     * @return
     */
    @PostMapping("/list")
    @ApiOperation(value = "分页系统消息接口")
    public ResultWeb<MessageDto<SysMessage>> getPage(@RequestBody SysMsgParemeter sysMsgParemeter){

        MessageDto<SysMessage> msgPage = sysMessageService.getPage(sysMsgParemeter);

        return msgPage.getTotal() > 0 ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"分页查询系统消息成功",msgPage)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"分页查询系统消息失败");
    }
}

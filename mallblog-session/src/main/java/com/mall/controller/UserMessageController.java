package com.mall.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.dto.MessageDto;
import com.mall.dto.MessagePage;
import com.mall.pojo.UserMessage;
import com.mall.service.UserMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "用户会话接口")
public class UserMessageController {

    @Autowired
    private UserMessageService userMessageService;

    @ApiOperation(value = "聊天锁设置")
    @GetMapping("/lock")
    public ResultWeb<String> lockTalk(@RequestParam Long sendId, @RequestParam Long getId){

        Boolean flag = userMessageService.lock(sendId,getId);

        return flag ? new ResultWeb<>(ResultCode.LOCK_SUCCESS,"聊天锁成功")
                : new ResultWeb<>(ResultCode.LOCK_ERROR,"聊天锁失败");
    }

    @GetMapping(value = {"/page/{id}","/page"})
    @ApiOperation(value = "管理端分页展示用户聊天信息接口")
    public ResultWeb<MessageDto<MessagePage>> getMsgPage(@PathVariable(value = "id",required = false) Long id, @RequestParam Integer pageNum, @RequestParam Integer pageSize){

        MessageDto<MessagePage> msgPage = userMessageService.getMsgPage(id,pageNum,pageSize);

        return msgPage.getTotal() > 0 ? new ResultWeb<>(ResultCode.QUERY_SUCCESS,"分页查询用户会话信息成功",msgPage)
                : new ResultWeb<>(ResultCode.QUERY_ERROR,"分页查询用户会话信息失败");
    }
}

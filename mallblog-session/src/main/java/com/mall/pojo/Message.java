package com.mall.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("se_message")
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;
    // 关联标识
    @ApiModelProperty("消息存储id")
    private Long contentId;
    @ApiModelProperty("发送人id")
    private Long sendId;
    // 消息类型 1-表示对方的消息，0-表示自己的消息
//    private Integer type;   // 重新思考逻辑后，觉得数据库中不需要存储该属性，应该封装在dto中
    // 消息发送时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("消息发送时间")
    private LocalDateTime time;
    // 消息类型
    @ApiModelProperty("消息的类型")
    private Integer msgType;
    // 消息
    @ApiModelProperty("消息")
    private String message;
    // 消息状态
    @ApiModelProperty("消息的状态")
    private Integer status;

}

package com.mall.pojo;

import lombok.Data;

@Data
public class SysMessageParam extends SysMessage{

    /**
    * @Description: type用来表示系统消息的类型：0-购买消息，1-警告信息
    * @Param:
    * @return:
    * @Author: @从零开始
    * @Date: 2023-10-18
    */

    private Integer type;
}

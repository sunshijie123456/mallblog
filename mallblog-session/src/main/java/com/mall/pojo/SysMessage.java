package com.mall.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@ApiModel("系统消息数据模型")
@TableName("se_sys_message")
public class SysMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    // 主键
    @ApiModelProperty("主键")
    private Long id;
    // 发送人
    @ApiModelProperty("发送人id")
    private Long addresserId;
    // 发送人昵称
    @ApiModelProperty("发送人昵称")
    private String addresserNickname;
    // 发送人类别  1-管理员，0-用户
    @ApiModelProperty("发送人类别")
    private String flag;
    // 接收人id
    @ApiModelProperty("接收人")
    private Long addresseeId;
    // 消息状态
    @ApiModelProperty("消息状态")
    private Integer status;
    // 信息
    @ApiModelProperty("消息内容")
    private String content;
    // 发送时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("消息发送时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

}

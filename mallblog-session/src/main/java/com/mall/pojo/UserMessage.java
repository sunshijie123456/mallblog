package com.mall.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("se_user_message")
public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    // 主键
    private Long id;
    // 发信人id
    private Long talkerA;
    // 发信人头像
    private String addresserPhoto;
    // 发信人昵称
    private String addresserNickname;
    // 收信人id
    private Long talkerB;
    // 最新消息的个数
    private Integer messageNumber;
    // 关联到消息存储表的id
    private Long contentId;
    // 会话状态
    private Integer status;
}

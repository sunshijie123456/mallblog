package com.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.dto.MessageList;
import com.mall.pojo.Message;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MessageService extends IService<Message> {

    /**
     * 保存用户的聊天信息
     * @param sendId
     * @param getId
     * @param status
     * @param message
     * @return
     */
    Boolean saveMsg(Long sendId,Long getId,Integer status,String message);

    /**
     * 查询消息列表
     * @param id
     * @return
     */
    List<MessageList> getMsgList(Long id);

    /**
     * 用户消息已读
     * @param userId
     * @param senderId
     * @return
     */
    Boolean readSenderMsg(Long userId, Long senderId);
}

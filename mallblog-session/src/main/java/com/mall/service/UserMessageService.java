package com.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.dto.MessageDto;
import com.mall.dto.MessagePage;
import com.mall.pojo.UserMessage;

import java.util.List;

public interface UserMessageService extends IService<UserMessage> {

    /**
     * 查询会话双方消息存储的id
     * @param sendId
     * @param getId
     * @return
     */
    Long getMsgId(Long sendId, Long getId);

    /**
     * 创建会话
     * @param sendId
     * @param getId
     * @return
     */
    Long createSession(Long sendId, Long getId);

    /**
     * 判断消息接受人是否正在与别人聊天
     * @param addressee
     * @return
     */
    Boolean chargeStatus(String addressee, String addresser);

    /**
     * 聊天锁设置
     * @param sendId
     * @param getId
     * @return
     */
    Boolean lock(Long sendId, Long getId);

    /**
     * 分页查询用户会话信息
     * @param id
     * @param pageNum
     * @param pageSize
     * @return
     */
    MessageDto<MessagePage> getMsgPage(Long id, Integer pageNum, Integer pageSize);
}

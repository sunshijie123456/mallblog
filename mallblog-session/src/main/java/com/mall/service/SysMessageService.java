package com.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.dto.MessageDto;
import com.mall.pojo.SysMessage;
import com.mall.pojo.SysMsgParemeter;

import java.util.List;

public interface SysMessageService extends IService<SysMessage> {

    /**
     * 发送系统消息
     * @param sysMessage
     * @return
     */
    Boolean sendMsg(SysMessage sysMessage);

    /**
     * 查询用户未读消息
     * @param id
     * @return
     */
    List<SysMessage> getMsgList(Long id);

    /**
     * 消息已读操作
     * @param ids
     * @return
     */
    Boolean updateStatus(String ids);

    /**
     * 分页查询系统消息
     * @param sysMsgParemeter
     * @return
     */
    MessageDto<SysMessage> getPage(SysMsgParemeter sysMsgParemeter);
}

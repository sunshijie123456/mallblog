package com.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.IPage;
import com.mall.dto.MessageDto;
import com.mall.exception.service.ex.NullKeywodsException;
import com.mall.mapper.SysMessageMapper;
import com.mall.pojo.SysMessage;
import com.mall.pojo.SysMsgParemeter;
import com.mall.service.SysMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SysMessageServiceImpl extends ServiceImpl<SysMessageMapper, SysMessage> implements SysMessageService {

    /**
    * @Description: 发送系统消息
    * @Param: sysMessage
    * @return:
    * @Author: @从零开始
    * @Date: 2023-10-18
    */
    @Override
    public Boolean sendMsg(SysMessage sysMessage) {

        // 初始为未读状态
        sysMessage.setStatus(0);

        return this.save(sysMessage);
    }

    /**
    * @Description: 查询用户未读消息
    * @Param:
    * @return:
    * @Author: @从零开始
    * @Date: 2023-10-18
    */
    @Override
    public List<SysMessage> getMsgList(Long id) {

        LambdaQueryWrapper<SysMessage> queryWrapper = new LambdaQueryWrapper<>();
        // 账号匹配
        queryWrapper.eq(id != null,SysMessage::getAddresseeId,id);
        queryWrapper.eq(SysMessage::getStatus,0);
        // 按时间升序查询
        queryWrapper.select().orderByDesc(SysMessage::getCreateTime);
        // 每次获取5条数据
        Page<SysMessage> page = new Page<>(1,5);
        Page<SysMessage> pagemsg = this.page(page, queryWrapper);

        return pagemsg.getRecords();
    }

    /**
     * 更新消息状态
     * @param ids
     * @return
     */
    @Override
    public Boolean updateStatus(String ids) {

        String s1 = ids.replace("\"", "");
        String s2 = s1.replace("[", "");
        String str = s2.replace("]", "");
        String[] split = str.split(",");
        Long[] longs = new Long[split.length];
        for (int i = 0; i < split.length; i++) {
            longs[i] = Long.valueOf(split[i]);
        }
        LambdaUpdateWrapper<SysMessage> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(SysMessage::getStatus,1);
        updateWrapper.in(SysMessage::getId,longs);

        boolean flag = this.update(updateWrapper);

        return flag;
    }

    /**
     * 分页系统消息业务实现
     * @param sysMsgParemeter
     * @return
     */
    @Override
    public MessageDto<SysMessage> getPage(SysMsgParemeter sysMsgParemeter) {

        Integer pageNum = sysMsgParemeter.getPageNum();
        Integer pageSize = sysMsgParemeter.getPageSize();

        if (pageNum == null || pageSize == null){
            throw new NullKeywodsException("当前页或者分页大小不能为空");
        }

        String flag = sysMsgParemeter.getFlag();
        Long getId = sysMsgParemeter.getGetId();
        Long sendId = sysMsgParemeter.getSendId();

        LambdaQueryWrapper<SysMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(!flag.equals(" ") && flag.length() > 0,SysMessage::getFlag,flag);
        queryWrapper.eq(sendId != null,SysMessage::getAddresserId,sendId);
        queryWrapper.eq(getId != null,SysMessage::getAddresseeId,getId);

        Page<SysMessage> page = new Page<>(pageNum,pageSize);
        Page<SysMessage> sysMessagePage = this.page(page,queryWrapper);

        MessageDto<SysMessage> messageDto = new MessageDto<>();
        messageDto.setTotal((int)sysMessagePage.getTotal());
        messageDto.setData(sysMessagePage.getRecords());

        return messageDto;
    }

}

package com.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.dto.MessageDto;
import com.mall.dto.MessagePage;
import com.mall.exception.service.ex.NormKeywordsException;
import com.mall.exception.service.ex.NullKeywodsException;
import com.mall.mapper.UserMessageMapper;
import com.mall.pojo.Message;
import com.mall.pojo.UserMessage;
import com.mall.service.MessageService;
import com.mall.service.UserMessageService;
import com.mall.utills.MyUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserMessageServiceImpl extends ServiceImpl<UserMessageMapper, UserMessage> implements UserMessageService {

    @Autowired
    private MessageService messageService;

    /**
     * 查询会话双方消息存储的id
     * @param sendId
     * @param getId
     * @return
     */
    @Override
    public Long getMsgId(Long sendId, Long getId) {

        // 对会话双方的id进行判空操作
        if (sendId == null || getId == null){
            throw new NullKeywodsException("会话双方id不能为空");
        }

        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        // 下述匹配情况 AA AB BA BB，其中 AB 或者 BA 满足我们的需求，
        // 在插入表时，可以对AA这种进行情况进行限制
        queryWrapper.in(UserMessage::getTalkerA,sendId,getId);
        queryWrapper.in(UserMessage::getTalkerB,sendId,getId);

        UserMessage userMessage = this.getOne(queryWrapper);

        return userMessage == null ? null : userMessage.getContentId();
    }

    /**
     * 创建会话
     * 会话双方id
     * @param sendId
     * @param getId
     * @return
     */
    @Override
    public Long createSession(Long sendId, Long getId) {

        // 判断id是否为空
        if (sendId == null || getId == null){
            throw new NullKeywodsException("会话双方id不能为空");
        }
        // 判断id是否一样
        if (sendId.equals(getId)){
            throw new NormKeywordsException("会话双方id不能一样");
        }

        UserMessage userMessage = new UserMessage();
        Long msgId = Long.parseLong(MyUUID.getGuid());
        userMessage.setTalkerA(sendId);
        userMessage.setTalkerB(getId);
        userMessage.setContentId(msgId);
        userMessage.setMessageNumber(0);
        boolean flag = this.save(userMessage);

        return flag ? msgId : null;
    }

    /**
     * 判断消息接受人是否正在与别人聊天
     * @param addressee 接受人
     * @param addresser 发送人
     * @return
     */
    @Override
    public Boolean chargeStatus(String addressee,String addresser) {

        Long getid = Long.parseLong(addressee);
        Long sendid = Long.parseLong(addresser);
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserMessage::getStatus,1)
                .eq(UserMessage::getTalkerA,getid)
                .or()
                .eq(UserMessage::getStatus,1)
                .eq(UserMessage::getTalkerB,getid);

        List<UserMessage> list = this.list(queryWrapper);

        if (list.size() == 0){
            return true;
        }else if (list.size() == 1){
            // 判断是否为自己
            UserMessage userMessage = list.get(0);
            Long talkerA = userMessage.getTalkerA();
            Long talkerB = userMessage.getTalkerB();

            return talkerA.equals(sendid) || talkerB.equals(sendid);
        }else {
            return false;
        }
    }

    /**
     * 聊天锁设置：上锁的目标是，当用户进入对话框时，其他用户将无法直接对其发送消息
     * 本质上是对用户本身上锁
     * @param sendId
     * @param getId
     * @return
     */
    @Override
    public Boolean lock(Long sendId, Long getId) {

        // 用户发送上锁请求，即当前状态下，与用户sendId交互只能是getId
        boolean flag = false;
        // 1.判断是否存在sendId之前的锁
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserMessage::getStatus,1)
                .eq(UserMessage::getTalkerA,sendId)
                .or()
                .eq(UserMessage::getStatus,1)
                .eq(UserMessage::getTalkerB,sendId);
        List<UserMessage> list = this.list(queryWrapper);

        LambdaUpdateWrapper<UserMessage> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(UserMessage::getStatus,1)
                .eq(UserMessage::getTalkerA,sendId)
                .eq(UserMessage::getTalkerB,getId)
                .or()
                .eq(UserMessage::getTalkerB,sendId)
                .eq(UserMessage::getTalkerA,getId);
        if (list.size() == 0){
            // 2.为sendId和getId上锁
            flag = this.update(updateWrapper);
        }else {
            // 解开之前的锁
            UserMessage userMessage = list.get(0);
            userMessage.setStatus(0);
            boolean update = this.updateById(userMessage);
            // 2.为sendId和getId上锁
            if (update){
                flag = this.update(updateWrapper);
            }
        }

        return flag;
    }

    /**
     * 分页查询用户会话信息
     * @param id
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public MessageDto<MessagePage> getMsgPage(Long id, Integer pageNum, Integer pageSize) {

        if (pageNum == null || pageSize == null){
            throw new NullKeywodsException("当前页或者分页大小不能为空");
        }

        MessageDto<MessagePage> messageDto = new MessageDto<>();

        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(id != null,UserMessage::getTalkerA,id)
                .or()
                .eq(id != null,UserMessage::getTalkerB,id);
        Page<UserMessage> page = new Page<>(pageNum,pageSize);
        Page<UserMessage> userMessagePage = this.page(page, queryWrapper);
        List<UserMessage> records = userMessagePage.getRecords();
        messageDto.setTotal((int)userMessagePage.getTotal());

        List<MessagePage> collect = records.stream().map((item) -> {
            MessagePage messagePageDto = new MessagePage();

            messagePageDto.setTalkerA(item.getTalkerA());
            messagePageDto.setTalkerB(item.getTalkerB());
            messagePageDto.setTalkerAName(item.getAddresserNickname());
//            messagePageDto.setTalkerBName();

            LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Message::getContentId, item.getContentId());
            wrapper.eq(id != null,Message::getSendId, id);
            wrapper.orderByDesc(Message::getTime);
            Page<Message> msgPage = new Page<>(1, 30);
            Page<Message> messagePage = messageService.page(msgPage, wrapper);
            List<Message> messages = messagePage.getRecords();
            messagePageDto.setNumber(messagePage.getTotal());

            if (messages.size() > 0){
                messagePageDto.setNewTime(messages.get(0).getTime());
                messagePageDto.setOldTime(messages.get(messages.size() - 1).getTime());
                messagePageDto.setMessages(messages);
            }

            return messagePageDto;
        }).collect(Collectors.toList());

        messageDto.setData(collect);
        return messageDto;
    }
}

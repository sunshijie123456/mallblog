package com.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.common.ResultWeb;
import com.mall.dto.MessageList;
import com.mall.dto.MsgDetail;
import com.mall.exception.service.ex.NullKeywodsException;
import com.mall.feign.ClientFeign;
import com.mall.feign.pojo.ComUser;
import com.mall.mapper.MessageMapper;
import com.mall.pojo.Message;
import com.mall.pojo.UserMessage;
import com.mall.service.MessageService;
import com.mall.service.UserMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

    @Autowired
    private UserMessageService userMessageService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private ClientFeign clientFeign;

    /**
     * 保存用户的聊天信息
     * @param sendId 发送人id
     * @param getId 接受者id
     * @param status 消息状态
     * @param message 消息内容
     * @return
     */
    @Override
    public Boolean saveMsg(Long sendId, Long getId, Integer status, String message) {

        // 获取消息存储的id
        Long msgId = userMessageService.getMsgId(sendId, getId);
        if (msgId == null){
            // 说明两者以前并未创建过会话，开始创建会话
            msgId = userMessageService.createSession(sendId,getId);
        }
        Message msg = new Message();
        msg.setMessage(message);
        msg.setStatus(status);
        msg.setTime(LocalDateTime.now());
        msg.setContentId(msgId);
        msg.setSendId(sendId);
        msg.setMsgType(0);

        boolean flag = this.save(msg);

        return flag;
    }

    /**
     * 查询消息列表
     * @param id
     * @return
     */
    @Override
    public List<MessageList> getMsgList(Long id) {

        // 查询列表消息存储id
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserMessage::getTalkerB,id).or()
                .eq(UserMessage::getTalkerA,id);

        List<UserMessage> userMessages = userMessageService.list(queryWrapper);

        List<MessageList> lists = userMessages.stream().map((item) -> {
            MessageList messageList = new MessageList();
            // 获取消息存储id
            Long msgId = item.getContentId();
            LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
            // 匹配消息存储id 匹配消息发送者非id 匹配未读消息
            wrapper.eq(Message::getContentId,msgId);
            wrapper.ne(Message::getSendId,id);
            wrapper.eq(Message::getStatus,0);
            // 按时间进行排序,新消息放在前面
            wrapper.orderByAsc(Message::getTime);
            List<Message> list = messageService.list(wrapper);

            // 判断list是否为空,只要为空说明关于此人没有未读消息
            if (list.size() == 0){
                messageList = null;
            }else {
                Long sendId = list.get(0).getSendId();
                ResultWeb<ComUser> user = clientFeign.getUserInfo(sendId,"mallblog");
                messageList.setUserId(sendId);
                messageList.setPhoto(user.getData().getPhoto());
                messageList.setUserName(user.getData().getNickName());
                messageList.setNumber(list.size());
                LocalDateTime time = list.get(0).getTime();
                messageList.setTime(time);
                // 封装详细消息
                List<MsgDetail> details = list.stream().map((msg) -> {
                    MsgDetail msgDetail = new MsgDetail();
                    msgDetail.setType(1);
                    msgDetail.setMsgType(0);
                    msgDetail.setTime(time);
                    msgDetail.setPhoto(user.getData().getPhoto());
                    msgDetail.setMessage(msg.getMessage());
                    return msgDetail;
                }).collect(Collectors.toList());

                messageList.setMsg(details);
            }
            return messageList;
        }).collect(Collectors.toList());

        // 去除为空的集合
        List<MessageList> stream = lists.stream().filter(Objects::nonNull).collect(Collectors.toList());

        return stream;
    }

    /**
     * 用户消息已读
     * @param userId
     * @param senderId
     * @return
     */
    @Override
    public Boolean readSenderMsg(Long userId, Long senderId) {

        if (userId == null || senderId == null){
            throw new NullKeywodsException("消息已读关键字不能为空");
        }
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(UserMessage::getTalkerA,userId).eq(UserMessage::getTalkerB,senderId)
                .or()
                .eq(UserMessage::getTalkerA,senderId).eq(UserMessage::getTalkerB,userId);

        UserMessage userMessage = userMessageService.getOne(queryWrapper);

        if (userMessage == null){
            // 并没有查询两人之间存在会话关系
            return false;
        }

        // 更新消息状态
        LambdaUpdateWrapper<Message> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(Message::getStatus,1)
                .eq(Message::getContentId,userMessage.getContentId())
                .eq(Message::getSendId,senderId);

        return this.update(updateWrapper);
    }
}

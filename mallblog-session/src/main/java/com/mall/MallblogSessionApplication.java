package com.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.mall")
@EnableFeignClients
public class MallblogSessionApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallblogSessionApplication.class, args);
    }

}

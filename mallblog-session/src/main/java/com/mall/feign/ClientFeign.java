package com.mall.feign;

import com.mall.common.ResultWeb;
import com.mall.feign.pojo.ComUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient("clientserver")
public interface ClientFeign {

    /**
     * 通过id获取用户的个人信息
     * @param userId
     * @return
     */
    @GetMapping("/client/{userId}")
    ResultWeb<ComUser> getUserInfo(@PathVariable Long userId, @RequestHeader(name = "from",required = true) String from);
}

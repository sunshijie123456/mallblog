package com.mall.session;

import com.mall.pojo.Message;
import com.mall.pojo.UserMessage;
import com.mall.service.MessageService;
import com.mall.service.UserMessageService;
import com.mall.utills.MyUUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * addresser : 消息发送者的id
 * addressee ： 消息接收者的id
 * 连接的名称，或者说线程是以发送者的id进行存储
 */

@Component
@Slf4j
@ServerEndpoint("/session/{addresser}/{addressee}")
public class WebSocket {

    /**
     * 线程安全的无序集合-希望每个连接相互独立
     */
    private static final CopyOnWriteArraySet<Session> SESSIONS = new CopyOnWriteArraySet<>();

    /**
     * 存储在线线程的数量
     */
    private static final Map<String,Session> SESSION_POOL = new HashMap<>();

    /**
     * websocket默认是多列，而spring是单列，两者冲突，因此使用方法注入
     */
    private static UserMessageService userMessageService;
    @Autowired
    public void setUserMessageService(UserMessageService userMessageService){
        WebSocket.userMessageService = userMessageService;
    }

    private static MessageService messageService;
    @Autowired
    public void MessageService(MessageService messageService){
        WebSocket.messageService = messageService;
    }

    @OnOpen
    public void onOpen(Session session, @PathParam(value = "addresser") String addresser, @PathParam(value = "addressee") String addressee){
//        String key = addresser + addressee;
        SESSIONS.add(session);
        SESSION_POOL.put(addresser,session);
        log.info("当前线程数量" + SESSIONS.size());
    }

    @OnClose
    public void onClose(Session session){
        SESSIONS.remove(session);
        log.info("连接终端,在线连接总数为:" + SESSIONS.size());
    }

    @OnMessage
    public void onMessage(String msg, @PathParam(value = "addresser") String addresser){
        String[] strings = msg.split("/");
        // 获取接受者id
        String addressee = strings[0];
        // 获取消息
        String message = strings[1];

        System.out.println("接受到用户" + addresser + "发送给：" + addressee + "的消息" + message);
        sendOneMessage(addresser,addressee,message);
//        sendAllMessage(msg,addresser);
    }

    /**
     * 私聊信息
     * @param addresser 发送人
     * @param addressee 接受人
     * @param message 消息内容
     */
    public void sendOneMessage(String addresser, String addressee, String message){
        // 对方只要不关闭网页，连接是不断的，那么如果用户在和其他的用户进行聊天，消息不应该直接发送过去
        // 而应该作为一个未读消息存储在数据库中。
        Session session = SESSION_POOL.get(addressee);
        if (session != null && session.isOpen()){
            // 对方在线
            // 但是对方正在别人进行聊天
            if (!userMessageService.chargeStatus(addressee,addresser)){
                // 保存消息到数据中
                messageService.saveMsg(Long.parseLong(addresser),Long.parseLong(addressee),0,message);
            }else{
                // 发送消息
                session.getAsyncRemote().sendText(message);
                // 保存信息
                messageService.saveMsg(Long.parseLong(addresser),Long.parseLong(addressee),1,message);
            }
        }else{
            // 不在线
            // 保存消息
            messageService.saveMsg(Long.parseLong(addresser),Long.parseLong(addressee),0,message);
        }
    }

    /**
     * 群发消息
     * @param msg
     */
    public void sendAllMessage(String msg, String key){
        // 排除本身的线程
        Session session_user = SESSION_POOL.get(key);
        for (Session session : SESSIONS) {
            if (!session.equals(session_user)){
                session.getAsyncRemote().sendText(msg);
            }
        }
    }
}

package com.mall.feign;

import com.mall.pojo.ComUser;
import com.mall.pojo.ResultWeb;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.cloud.openfeign.FeignClient("clientserver")
public interface ClientFeign {

    @GetMapping("/client/login")
    ResultWeb<ComUser> LoginUser(@RequestParam String username, @RequestParam String password, @RequestParam String code,
                                    @RequestParam String key, @RequestHeader(name = "from",required = true) String from);

}

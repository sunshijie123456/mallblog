package com.mall.pojo;

import lombok.Data;

@Data
public class ResultWeb<T> {
    private Integer code;
    private String msg;
    private T data;

    private String token;

    public ResultWeb(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultWeb(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultWeb() {
    }
}

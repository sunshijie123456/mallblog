package com.mall.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComUser {
    private Long userId;
    private String username;
    private String password;
    private String learnNum;
    private String nickName;
    private String phone;
    private String email;
    private Integer sex;
    private String photo;
    private String address;
    //帐户是否可用(1 可用，0 删除用户)
    private Integer isEnabled;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;
    private Integer status;
}

package com.mall.login.impl;

import com.mall.feign.ClientFeign;
import com.mall.login.LoginService;
import com.mall.pojo.ComUser;
import com.mall.pojo.ResultWeb;
import com.mall.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LoginServiceImpl implements LoginService {


    @Autowired
    private ClientFeign clientFeign;

    /**
     * 登录验证
     * @param username
     * @param password
     * @param code
     * @return
     */
    @Override
    public ResultWeb<ComUser> login(List<String> username, List<String> password, List<String> code, List<String> key) {

        String userName = username.get(0);
        String ps = password.get(0);
        String cod = code.get(0);
        String codekey = key.get(0);
        ResultWeb<String> web = new ResultWeb<>();

        ResultWeb<ComUser> result = clientFeign.LoginUser(userName, ps, cod,codekey,"mallblog");

        if (result.getData() != null){
            String token = JWTUtils.getToken(userName, ps);
            result.setMsg("登录成功");
//            web.setCode(ResultCode.LOGIN_SUCCESS);
            result.setToken(token);

            return result;
        }

        result.setMsg("登录失败");
        return result;
    }
}

package com.mall.login;

import com.mall.pojo.ComUser;
import com.mall.pojo.ResultWeb;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.List;

public interface LoginService {
    // 登录验证
    ResultWeb<ComUser> login(List<String> username, List<String> password, List<String> code, List<String> key);
}

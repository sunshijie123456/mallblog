package com.mall.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;

public class JWTUtils {

    // 登录密钥
    private static final String SIGN = "logining";

    /**
     * 生成token
     * @param username
     * @param password
     * @return
     */
    public static String getToken(String username, String password){

        Calendar instance = Calendar.getInstance();
        // 用户没有自动退出的话，有效的登录时间是1天
        instance.add(Calendar.DATE,1);
        // 创建jwt builder
        JWTCreator.Builder builder = JWT.create();
        String token = builder.withClaim("username", username)  // 设置payload中的内容
                .withClaim("password", password)
                .withExpiresAt(instance.getTime())  // 设置过期时间
                .sign(Algorithm.HMAC256(SIGN));     // 设置登录登录成功后的标识

        return token;
    }

    /**
     * 验证token的合法性
     * @param token
     * @return
     */
    public static DecodedJWT verify(String token){
        return JWT.require(Algorithm.HMAC256(SIGN)).build().verify(token);
    }

}

package com.mall.RabbitMq.controller;

import com.mall.RabbitMq.pojo.MqParameter;
import com.mall.RabbitMq.service.RabbitMqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("rabbitmq")
public class RabbitMqController {

    @Autowired
    private RabbitMqService rabbitMqService;

    /**
     * 实现调用接口向rabbitmq发送信息
     * @param parameter
     */
    @PostMapping
    public void SendMessage(@RequestBody MqParameter parameter){

        // 获取相关的参数
        String rounterkey = parameter.getRounterkey();
        String exchangeName = parameter.getExchangeName();
        String queueName = parameter.getQueueName();
        String msg = parameter.getMsg();

        // 判断参数是否为空
        if (rounterkey != null){
            rabbitMqService.sendMessageToDesignatedConsumer(exchangeName,rounterkey,msg);
            return;
        }

        if (exchangeName != null){
            rabbitMqService.sendMessageToQueuesByExchange(exchangeName,msg);
            return;
        }

        rabbitMqService.sendMessageToQueue(queueName,msg);
    }
}

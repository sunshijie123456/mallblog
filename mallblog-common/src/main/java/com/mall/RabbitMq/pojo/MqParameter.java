package com.mall.RabbitMq.pojo;

import lombok.Data;

@Data
public class MqParameter {

    private String queueName;
    private String exchangeName;
    private String Rounterkey;
    private String msg;
}

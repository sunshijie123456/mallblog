package com.mall.RabbitMq.service.impl;

import com.mall.RabbitMq.service.RabbitMqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@Service
public class RabbitMqServiceImpl implements RabbitMqService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 直接向队列发送消息
     * @param queue
     * @param msg
     */
    @Override
    public void sendMessageToQueue(String queue, String msg) {
        rabbitTemplate.convertAndSend(queue,msg);
    }

    /**
     * 通过交换机向多个队列发送消息
     * @param exchange
     * @param msg
     */
    @Override
    public void sendMessageToQueuesByExchange(String exchange, String msg) {
        rabbitTemplate.convertAndSend(exchange,"",msg);
    }

    /**
     * 通过交换机向指定的消费者发送消息
     * @param exchange
     * @param routerKey
     * @param msg
     */
    @Override
    public void sendMessageToDesignatedConsumer(String exchange, String routerKey, String msg) {
        rabbitTemplate.convertAndSend(exchange,routerKey,msg);
    }
}

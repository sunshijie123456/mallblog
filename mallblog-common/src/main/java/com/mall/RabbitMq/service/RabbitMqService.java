package com.mall.RabbitMq.service;

public interface RabbitMqService {

    // 直接将消息发送到制定的队列中
    void sendMessageToQueue(String queue, String msg);

    // 利用交换机将消息发送给多个队列中
    void sendMessageToQueuesByExchange(String exchange, String msg);

    // 利用路由键将消息发送到制定消费者
    void sendMessageToDesignatedConsumer(String exchange, String routerKey, String msg);
}

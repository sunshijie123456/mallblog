package com.mall.publisher;

public interface RabbitMqPublisher {

    /**
     * 发送消息更新Es中的商品信息
     * @param msg
     */
    void sendMsgWithProductToEs(String msg);

    /**
     * 发送消息更新redis中的商品信息
     * @param msg
     */
    void sendMsgWithProductToRedis(String msg);

    /**
     * 设置会员到期时间
     * @param id
     */
    void setMemberDeadLine(Long id, Integer type);
}

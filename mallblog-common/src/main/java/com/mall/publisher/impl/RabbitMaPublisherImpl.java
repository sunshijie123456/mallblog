package com.mall.publisher.impl;

import com.mall.common.ResultCode;
import com.mall.publisher.RabbitMqPublisher;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
* @Description: rabbitmq消息发送封装
* @Param:
* @return:
* @Author: @从零开始
* @Date: 2023-11-21
*/

@Component
public class RabbitMaPublisherImpl implements RabbitMqPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息到es商品队列
     * @param msg
     */
    @Override
    public void sendMsgWithProductToEs(String msg) {
        rabbitTemplate.convertAndSend(ResultCode.PRODUCT_EXCHANGE,ResultCode.ES_ROUYINGKEY,msg);
    }

    /**
     * 发送消息到redis商品队列
     * @param msg
     */
    @Override
    public void sendMsgWithProductToRedis(String msg) {
        rabbitTemplate.convertAndSend(ResultCode.PRODUCT_EXCHANGE,ResultCode.REDIS_ROUYINGKEY,msg);
    }

    /**
     * 设置会员到期时间
     * @param id
     * @param type
     */
    @Override
    public void setMemberDeadLine(Long id, Integer type) {

        long time = 10000L;
        if (type == 0){
            time = 2678400000L; // 31 * 24 * 60 * 60 * 1000
        }else if (type == 1){
            time = 8035200000L; // (31 * 24 * 60 * 60 * 1000) * 3
        }else {
            time = 32140800000L;    // (31 * 24 * 60 * 60 * 1000) * 3 * 4
        }

        Message message = MessageBuilder.withBody(id.toString().getBytes(StandardCharsets.UTF_8))
                .setHeader("x-delay", time)    // 毫秒
                .build();

        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());

        rabbitTemplate.convertAndSend(ResultCode.DELAY_EXCHANGE,ResultCode.DELAY_ROUTINGKEY,message,correlationData);
    }
}

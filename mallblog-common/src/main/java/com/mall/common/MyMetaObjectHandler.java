package com.mall.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.util.Random;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        // 数据插入时，实现相关功能
        metaObject.setValue("createTime", LocalDateTime.now());
//        metaObject.setValue("updateTime", LocalDateTime.now());
//        metaObject.setValue("createUser", new Random().nextLong());
//        metaObject.setValue("updateUser", new Random().nextLong());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 数据更新时，实现数据填充
        metaObject.setValue("updateTime", LocalDateTime.now());
    }
}

package com.mall.common;

import lombok.Data;

@Data
public class QueryParameters {

    private String name;
    private String category;
    private String type;
    private String brand;

    private Integer pageSize;
    private Integer page;

}

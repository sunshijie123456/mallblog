package com.mall.common;

public class ResultCode {

    // 插入操作的返回码
    public static final Integer INSERT_SUCCESS = 10011;
    public static final Integer INSERT_ERROR = 10010;

    // 更新操作的返回码
    public static final Integer UPDATE_SUCCESS = 10021;
    public static final Integer UPDATE_ERROR = 10020;

    // 删除操作的返回码
    public static final Integer DELETE_SUCCESS = 10031;
    public static final Integer DELETE_ERROR = 10030;

    // 查询操作的返回码
    public static final Integer QUERY_SUCCESS = 10041;
    public static final Integer QUERY_ERROR = 10040;

    // 登录操作的返回码
    public static final Integer LOGIN_SUCCESS = 10051; //登录成功
    public static final Integer LOGIN_ERROR = 10050;    //登录失败

    // 验证码的全局名称
    public static final String VALIDATECODE = "validateCode";

    // es商品索引库的全局名称
    public static final String INDEXNAME = "commodity";

    // redis 相关常量
    // 初始化商品筛选条件存储时长---天数
    public static final Integer REDIS_DAY = 1;
    // 动态筛选条件存储时长---分钟
    public static final Integer REDIS_MINUTE = 20;

    // 前端数值传递错误
    public static final Integer ERROR_FRONT = 400;

    // 聊天锁设置返回结果
    public static final Integer LOCK_SUCCESS = 20001;
    public static final Integer LOCK_ERROR = 20000;

    // 全局异常返回错误码
    public static final Integer SERVICE_EXCEPTION = 20010;
    public static final Integer MAPPER_EXCEPTION = 20011;

    // 异步通信-交换机与队列名称
    public static final String PRODUCT_EXCHANGE = "product.direct";
    public static final String ES_PRODUCT = "es.product.queue";
    public static final String REDIS_PRODUCT = "redis.product.queue";

    // 路由键名称
    public static final String ES_ROUYINGKEY = "es";
    public static final String REDIS_ROUYINGKEY = "redis";

    // 延迟交换机、队列以及路由键名称
    public static final String DELAY_EXCHANGE = "delay.exchange";
    public static final String DELAY_QUEUE = "delay.queue";
    public static final String DELAY_ROUTINGKEY = "delay";

}

package com.mall.utills;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class ValidateCodeUtills {

    // 原始的字符串
    private final String str = "0123456789qwertyuipasdfghjkzxcvbnmQWERTYUPASDFGHJKLZXCVBNM";
    private char[] ch = str.toCharArray();
    private Integer width = 150;
    private Integer height = 50;
    private Random random = new Random();

    public String strCode;

    /**
     * 随机生成三色RGB
     * @return
     */
    public Color getColor(){
        int r = new Random().nextInt(255);
        int g = new Random().nextInt(255);
        int b = new Random().nextInt(255);
        return new Color(r,g,b);
    }

    /**
     * 提供获取字符串的接口
     * @return
     */
    public String getStrCode() {
        return strCode;
    }

    /**
     * 生成随机图片，转递给前端
     * @return
     */
    public BufferedImage getRandCode(){

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);

        Graphics graphics = image.getGraphics();
        graphics.fillRect(0,0,width,height);
        graphics.setColor(getColor());
        graphics.setFont(new Font("楷体",Font.PLAIN,25));

        // 生成字符串
        StringBuilder stringBuilder = new StringBuilder();
        int x = 25;
        int y = 35;
        for (int i = 0; i < 4; i++) {
            int index = random.nextInt(ch.length);
            graphics.drawString(String.valueOf(ch[index]),x,y);
            x += 25;
            stringBuilder.append(ch[index]);
        }
        strCode = stringBuilder.toString();

        // 生成干扰线
        graphics.setColor(getColor());
        for (int i = 0; i < 5; i++) {
            int x1 = random.nextInt(30);
            int y1 = random.nextInt(50);
            int x2 = random.nextInt(30) + 120;
            int y2 = random.nextInt(50);
            graphics.drawLine(x1,y1,x2,y2);
        }
        graphics.dispose();

//        try {
//            ImageIO.write(image,"jpg",new File("E:\\vs-code\\vue-next-admin-master\\image\\aa.jpg"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return image;

    }

    public static void main(String[] args) {
        ValidateCodeUtills validateCodeUtills = new ValidateCodeUtills();
        validateCodeUtills.getRandCode();
    }
}

package com.mall.exception.service.ex;

import com.mall.exception.service.ServiceException;

/**
* @Description: 文件大小超出最大值异常
* @Param:
* @return:
* @Author: @从零开始
* @Date: 2023-11-14
*/

public class FileSizeLimitException extends ServiceException {
    public FileSizeLimitException(String msg) {
        super(msg);
    }
}

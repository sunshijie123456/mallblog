package com.mall.common_user.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("com_user")
public class ComUser {
    @TableId(type = IdType.AUTO)
    private Long userId;
    private String username;
    private String password;
    private String learnNum;
    private String nickName;
    private String phone;
    private String email;
    private Integer sex;
    private String photo;
    private String address;
    //帐户是否可用(1 可用，0 删除用户)
    private Integer isEnabled;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;
    private Integer status;
    private Integer isVip;
}

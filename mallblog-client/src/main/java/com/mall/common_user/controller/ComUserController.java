package com.mall.common_user.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.common_user.dto.ResultPageDto;
import com.mall.common_user.pojo.PageParm;
import com.mall.common_user.pojo.ComUser;
import com.mall.common_user.service.ComUserService;
import com.mall.sys_user.pojo.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("/client")
@Api(tags = "普通用户账户操作接口")
public class ComUserController {
    @Autowired
    private ComUserService comUserService;

    //新增用户
    @PostMapping("/save")
    @ApiOperation("新增用户")
    public ResultWeb<String> addUser(@RequestBody ComUser user) {
        //判断账户是否被占用
        LambdaQueryWrapper<ComUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComUser::getUsername, user.getUsername());
        ComUser one = comUserService.getOne(queryWrapper);
        if (one != null) {
            return new ResultWeb<>(ResultCode.INSERT_ERROR, "账户被占用");
        }
        //密码加密
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        //管理员管理系统内新增用户默认已审核
        user.setStatus(2);

        user.setCreateTime(new Date());
        //入库
        boolean save = comUserService.save(user);
        if (save) {
            return new ResultWeb<>(ResultCode.INSERT_SUCCESS, "新增用户成功");
        }
        return new ResultWeb<>(ResultCode.INSERT_ERROR, "新增用户失败");
    }

    //编辑用户
    @PutMapping(value = {"/update/{flag}","/update/"})
    @ApiOperation("编辑用户")
    public ResultWeb<String> editUser(@RequestBody ComUser user, @PathVariable(value = "flag",required = false) Integer flag) {
        //判断账户是否被占用
        LambdaQueryWrapper<ComUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComUser::getUsername, user.getUsername());
        ComUser one = comUserService.getOne(queryWrapper);
        if (one != null && one.getUserId() != user.getUserId()) {
            return new ResultWeb<>(ResultCode.INSERT_ERROR, "账户被占用");
        }
        //密码加密
        if (user.getPassword() != null && flag == 1){
            user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        }else {
            // 当更新其他信息，应该设置密码为空，避免更新密码
            user.setPassword(null);
        }
        user.setUpdateTime(new Date());
        //更新信息
        boolean b = comUserService.updateById(user);
        if (b) {
            return new ResultWeb<>(ResultCode.UPDATE_SUCCESS, "编辑用户成功");
        }
        return new ResultWeb<>(ResultCode.UPDATE_ERROR, "编辑用户失败");
    }

    //删除用户
    @DeleteMapping("/{userId}")
    @ApiOperation("删除用户")
    public ResultWeb<String> deleteUser(@PathVariable("userId") Long userId){
        boolean b = comUserService.removeById(userId);
        if (b) {
            return new ResultWeb<>(ResultCode.DELETE_SUCCESS, "删除用户成功");
        }
        return new ResultWeb<>(ResultCode.DELETE_ERROR, "删除用户失败");
    }

    //列表查询
    @GetMapping("/list")
    @ApiOperation("列表查询")
    public ResultWeb<ResultPageDto<ComUser>> getList(PageParm parm){
        IPage<ComUser> list = comUserService.list(parm);
        //密码不显示
        list.getRecords().stream().forEach(item ->{
            item.setPassword("");
        });
        // 数据封装
        ResultPageDto<ComUser> pageDto = new ResultPageDto<>();
        pageDto.setData(list.getRecords());
        pageDto.setTotal((int)list.getTotal());

        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功",pageDto);
    }

    /**
     * 审核普通用户账号
     * @param status
     * @param id
     * @return
     */
    @PostMapping("/check/{status}")
    @ApiOperation("审核普通用户账号")
    public ResultWeb<String> checkComUser(@PathVariable Integer status,Long id){
        ComUser comUser =ComUser.builder()
                .status(status)
                .userId(id)
                .build();
        comUserService.updateById(comUser);
        return new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"审核通过!");
    }

    /**
     * 用户登录验证
     * @param username
     * @param password
     * @param code
     * @param key
     * @param from
     * @return
     */
    @GetMapping("/login")
    @ApiOperation("用户账号登录验证")
    public ResultWeb<ComUser> login(@RequestParam String username, @RequestParam String password, @RequestParam String code,
                                    @RequestParam String key,String from){

        return comUserService.login(username,password,code,key);
    }

    /**
     * 注册用户账号
     * @param request
     * @param username
     * @param password
     * @param code
     * @return
     */
    @GetMapping("/register")
    @ApiOperation("用户账号注册验证")
    public ResultWeb<String> register(HttpServletRequest request, @RequestParam String username, @RequestParam String password, @RequestParam String code){

        return comUserService.register(username,password,code);
    }

    /**
     * 获取用户的个人信息
     * @param userId
     * @return
     */
    @GetMapping("/{userId}")
    public ResultWeb<ComUser> getUserById(@PathVariable Long userId){

        ComUser user = comUserService.getById(userId);

        if (user != null) {
            return new ResultWeb<>(ResultCode.QUERY_SUCCESS, "查询用户成功",user);
        }
        return new ResultWeb<>(ResultCode.QUERY_ERROR, "查询用户失败");
    }

}

package com.mall.common_user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.common_user.pojo.ComUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ComUserMapper extends BaseMapper<ComUser> {
}

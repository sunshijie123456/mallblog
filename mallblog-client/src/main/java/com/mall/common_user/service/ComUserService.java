package com.mall.common_user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.common.ResultWeb;
import com.mall.common_user.pojo.PageParm;
import com.mall.common_user.pojo.ComUser;
import com.mall.sys_user.pojo.SysUser;

import javax.servlet.http.HttpServletRequest;

public interface ComUserService extends IService<ComUser> {
    IPage<ComUser> list(PageParm parm);

    // 验证用户登录
    ResultWeb<ComUser> login(HttpServletRequest request,String username, String password, String code);

    // 重载方法
    ResultWeb<ComUser> login(String username, String password, String code, String key);

    // 用户账号注册
    ResultWeb<String> register(String username, String password, String code);

    // 上传用户头像
    void uploadPhoto(String url, Long id);

    // 更新用户的会员状态
    void updateMemberStatus(Long id);

}

package com.mall.common_user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.common_user.mapper.ComUserMapper;
import com.mall.common_user.pojo.PageParm;
import com.mall.common_user.pojo.ComUser;
import com.mall.common_user.service.ComUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Service
public class ComUserServiceImpl extends ServiceImpl<ComUserMapper, ComUser> implements ComUserService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public IPage<ComUser> list(PageParm parm) {
        //构造分页对象
        IPage<ComUser> page = new Page<>();
        page.setSize(parm.getPageSize());
        page.setCurrent(parm.getPageNum());
        //构造查询条件
        LambdaQueryWrapper<ComUser> queryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotEmpty(parm.getNickName())){
            queryWrapper.like(ComUser::getNickName,parm.getNickName());
        }
        return this.baseMapper.selectPage(page,queryWrapper);
    }

    /**
     * 验证客户端用户登录
     * @param request
     * @param username 账号
     * @param password 密码
     * @param code 验证码
     * @return
     */
    @Override
    public ResultWeb<ComUser> login(HttpServletRequest request,String username, String password, String code) {

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR, "用户名或密码不能为空!");
        }

        //加密后的密码
        String s = DigestUtils.md5DigestAsHex(password.getBytes());

        //验证码验证
        HttpSession session = request.getSession();
//        String uid = (String) session.getAttribute(ResultCode.VALIDATECODE);
        String uid = (String)request.getServletContext().getAttribute(ResultCode.VALIDATECODE);
        if(!uid.equalsIgnoreCase(code)){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"验证码错误!");
        }

        //根据用户名和密码查询
        LambdaQueryWrapper<ComUser> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(ComUser::getUsername,username)
                .eq(ComUser::getPassword, s);
        ComUser one = this.getOne(queryWrapper);
        if(one == null){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"用户名或密码错误!");
        }else if(one.getStatus() != 2){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"账号审核中!");
        }else if (one.getIsEnabled() != 1){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"该账号已被禁用!");
        }
//        //返回数据给前端
//        LoginResult result = new LoginResult();
//        //result.setToken(String.valueOf(one.getUserId()));
//        result.setUserId(one.getUserId());
//        result.setNickName(one.getNickName());
//        result.setIsAdmin(one.getIsAdmin());
//        result.setPhoto(one.getPhoto());

        return new ResultWeb<>(ResultCode.LOGIN_SUCCESS,"登录成功！",one);
//        return null;
    }

    /**
     * 验证客户端用户登录-采用redis存储验证码
     * @param username 账号
     * @param password 密码
     * @param code 验证码
     * @return
     */
    @Override
    public ResultWeb<ComUser> login(String username, String password, String code, String key) {

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR, "用户名或密码不能为空!");
        }

        //加密后的密码
        String s = DigestUtils.md5DigestAsHex(password.getBytes());

        //验证码验证
        String uid = redisTemplate.opsForValue().get(key);
//        HttpSession session = request.getSession();
////        String uid = (String) session.getAttribute(ResultCode.VALIDATECODE);
//        String uid = (String)request.getServletContext().getAttribute(ResultCode.VALIDATECODE);
        if(uid != null && !uid.equalsIgnoreCase(code)){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"验证码错误!");
        }

        //根据用户名和密码查询
        LambdaQueryWrapper<ComUser> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(ComUser::getUsername,username)
                .eq(ComUser::getPassword, s);
        ComUser one = this.getOne(queryWrapper);
        if(one == null){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"用户名或密码错误!");
        }else if(one.getStatus() != 2){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"账号审核中!");
        }else if (one.getIsEnabled() != 1){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"该账号已被禁用!");
        }
//        //返回数据给前端
//        LoginResult result = new LoginResult();
//        //result.setToken(String.valueOf(one.getUserId()));
//        result.setUserId(one.getUserId());
//        result.setNickName(one.getNickName());
//        result.setIsAdmin(one.getIsAdmin());
//        result.setPhoto(one.getPhoto());

        return new ResultWeb<>(ResultCode.LOGIN_SUCCESS,"登录成功！",one);
//        return null;
    }

    @Override
    public ResultWeb<String> register(String username, String password, String code) {

        //判断账户是否被占用
        LambdaQueryWrapper<ComUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComUser::getUsername, username);
        ComUser one = this.getOne(queryWrapper);
        if (one != null) {
            return new ResultWeb<>(ResultCode.INSERT_ERROR, "账户被占用!");
        }
        //密码加密
        ComUser user = new ComUser();
        user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        //设置是否是超级管理员
        //user.setIsAdmin(1);
        user.setUsername(username);
        user.setIsEnabled(1);
        user.setStatus(1);  //注册默认未审核，需要超级管理员审核
        user.setCreateTime(new Date());
        //入库
        boolean save = this.save(user);
        if (save) {
            return new ResultWeb<>(ResultCode.INSERT_SUCCESS, "注册用户成功,请等待管理员审核....");
        }
        return new ResultWeb<>(ResultCode.INSERT_ERROR, "注册用户失败，请重试!");
    }

    /**
     * 上传用户头像
     * @param url
     */
    @Override
    public void uploadPhoto(String url, Long id) {

        // 根据用户id更新头像名称
        LambdaUpdateWrapper<ComUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(ComUser::getPhoto,url)
                .eq(ComUser::getUserId,id);

        this.update(updateWrapper);
    }

    /**
     * 更新用户的会员状态
     * @param id
     */
    @Override
    public void updateMemberStatus(Long id) {
        // 根据用户id更新头像名称
        LambdaUpdateWrapper<ComUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(ComUser::getIsVip,0)
                .eq(ComUser::getUserId,id);

        this.update(updateWrapper);
    }
}

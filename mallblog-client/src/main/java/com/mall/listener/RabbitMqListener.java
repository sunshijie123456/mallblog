package com.mall.listener;

import com.mall.common.ResultCode;
import com.mall.common_user.service.ComUserService;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
* @Description: 监听用户会员的过期时间
* @Param:
* @return:
* @Author: @从零开始
* @Date: 2023-11-21
*/

@Component
public class RabbitMqListener {

    @Autowired
    private ComUserService comUserService;

    /**
     * 监听用户会员的过期时间
     * 这里消息队列持久化存储，因为需要保存消息内容
     * @param msg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = ResultCode.DELAY_QUEUE,durable = "true"),
            exchange = @Exchange(name = ResultCode.DELAY_EXCHANGE,delayed = "true"),
            key = ResultCode.DELAY_ROUTINGKEY
    ))
    public void listenMemberDeadLine(String msg){
        // 接受到消息，更新数据库中的会员状态
        comUserService.updateMemberStatus(Long.parseLong(msg));
    }
}

package com.mall.login.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.login.entity.LoginParm;
import com.mall.login.entity.LoginVO;
import com.mall.login.entity.PersonMsg;
import com.mall.sys_user.pojo.SysUser;
import com.mall.sys_user.service.SysUserService;
import com.mall.utills.ValidateCodeUtills;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class LoginController {
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    //用户登录
    @PostMapping("/login")
    public ResultWeb<LoginVO> login(HttpServletRequest request, @RequestBody LoginParm loginParm){
        if(StringUtils.isEmpty(loginParm.getUsername()) || StringUtils.isEmpty(loginParm.getPassword())){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR, "用户名或密码不能为空!");
        }
        //加密后的密码
        String s = DigestUtils.md5DigestAsHex(loginParm.getPassword().getBytes());

        //验证码验证
        HttpSession session = request.getSession();
        String uid = (String) session.getAttribute(ResultCode.VALIDATECODE);
        if(!uid.equalsIgnoreCase(loginParm.getCode())){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"验证码错误!");
        }

        //根据用户名和密码查询
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(SysUser::getUsername,loginParm.getUsername())
                .eq(SysUser::getPassword, s);
        SysUser one = sysUserService.getOne(queryWrapper);
        if(one == null){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"用户名或密码错误!");
        }else if(one.getStatus() != 2){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"账号审核中!");
        }else if (one.getIsEnabled() != 1){
            return new ResultWeb<>(ResultCode.LOGIN_ERROR,"该账号已被禁用!");
        }

        LoginVO loginVO = LoginVO.builder()
                .userId(one.getUserId())
                .username(one.getUsername())
                .nickName(one.getNickName())
                .build();

        return new ResultWeb<>(ResultCode.LOGIN_SUCCESS,"登录成功！",loginVO);
    }

}

package com.mall.login.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PersonMsg {
    private Long userId;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Integer sex;
    private Integer isAdmin;
    private String nickName;
    private String photo;
}

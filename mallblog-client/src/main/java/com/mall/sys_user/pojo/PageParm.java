package com.mall.sys_user.pojo;

import lombok.Data;

@Data
public class PageParm {
    private Long pageNum;
    private Long pageSize;
    private String nickName;
}

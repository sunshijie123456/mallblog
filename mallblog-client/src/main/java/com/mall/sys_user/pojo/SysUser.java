package com.mall.sys_user.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user")
public class SysUser {
    @TableId(type = IdType.AUTO)
    private Long userId;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Integer sex;
    private Integer isAdmin;
    //帐户是否可用(1 可用，0 删除用户)
    private Integer isEnabled;
    private String nickName;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;
    private String photo;
    //账户审核状态（1 待审核 2 已审核）
    private Integer status;

}

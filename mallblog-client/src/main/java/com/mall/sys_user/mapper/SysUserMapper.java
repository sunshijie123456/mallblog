package com.mall.sys_user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.sys_user.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
}

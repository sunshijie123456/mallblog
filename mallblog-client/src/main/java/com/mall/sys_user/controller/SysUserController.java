package com.mall.sys_user.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.sys_user.dto.ResultPageDto;
import com.mall.sys_user.pojo.PageParm;
import com.mall.sys_user.pojo.SysUser;
import com.mall.sys_user.service.SysUserService;
import com.sun.prism.impl.BaseContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/role")
@Api(tags = "管理员账户操作接口")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    //用户注册
    @GetMapping("/register")
    @ApiOperation("用户注册")
    public ResultWeb<String> register(SysUser user) {
        //判断账户是否被占用
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUsername, user.getUsername());
        SysUser one = sysUserService.getOne(queryWrapper);
        if (one != null) {
            return new ResultWeb<>(ResultCode.INSERT_ERROR, "账户被占用!");
        }
        //密码加密
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        //设置是否是超级管理员
        //user.setIsAdmin(1);
        user.setIsEnabled(1);
        user.setStatus(1);  //注册默认未审核，需要超级管理员审核
        user.setCreateTime(new Date());
        //入库
        boolean save = sysUserService.save(user);
        if (save) {
            return new ResultWeb<>(ResultCode.INSERT_SUCCESS, "注册用户成功,请返回登录!");
        }
        return new ResultWeb<>(ResultCode.INSERT_ERROR, "注册用户失败!");
    }

    //新增用户
    @PostMapping("/save")
    @ApiOperation("新增用户")
    public ResultWeb<String> addUser(@RequestBody SysUser user) {
        //判断账户是否被占用
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUsername, user.getUsername());
        SysUser one = sysUserService.getOne(queryWrapper);
        if (one != null) {
            return new ResultWeb<>(ResultCode.INSERT_ERROR, "账户被占用!");
        }
        //密码加密
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        //设置是否是超级管理员
        //user.setIsAdmin(1);
        user.setStatus(2);
        user.setCreateTime(new Date());
        //入库
        boolean save = sysUserService.save(user);
        if (save) {
            return new ResultWeb<>(ResultCode.INSERT_SUCCESS, "新增用户成功!");
        }
        return new ResultWeb<>(ResultCode.INSERT_ERROR, "新增用户失败!");
    }

    //编辑用户
    @PutMapping(value = {"/update","/update/{status}"})
    @ApiOperation("编辑用户")
    public ResultWeb<String> editUser(@RequestBody SysUser user,@PathVariable(required = false) Integer status) {
        if (status == null) {
            //判断账户是否被占用
            LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(SysUser::getUsername, user.getUsername());
            SysUser one = sysUserService.getOne(queryWrapper);
            if (one != null && one.getUserId() != user.getUserId()) {
                return new ResultWeb<>(ResultCode.INSERT_ERROR, "账户被占用!");
            }
            //密码加密
            user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
            user.setUpdateTime(new Date());
            //更新信息
            boolean b = sysUserService.updateById(user);
            if (b) {
                return new ResultWeb<>(ResultCode.UPDATE_SUCCESS, "编辑用户成功!");
            }
            return new ResultWeb<>(ResultCode.UPDATE_ERROR, "编辑用户失败!");
        } else {
            SysUser sysUser = SysUser.builder()
                    .isEnabled(status)
                    .userId(user.getUserId())
                    .updateTime(new Date())
                    .build();
            sysUserService.updateById(sysUser);
            return new ResultWeb<>(ResultCode.UPDATE_SUCCESS, "修改用户状态成功!");
        }
    }

    //删除用户
    @DeleteMapping("/{userId}")
    @ApiOperation("删除用户")
    public ResultWeb<String> deleteUser(@PathVariable("userId") Long userId){
        boolean b = sysUserService.removeById(userId);
        if (b) {
            return new ResultWeb<>(ResultCode.DELETE_SUCCESS, "删除用户成功!");
        }
        return new ResultWeb<>(ResultCode.DELETE_ERROR, "删除用户失败!");
    }

    //列表查询
    @GetMapping("/list")
    @ApiOperation("分页查询")
    public ResultWeb<ResultPageDto<SysUser>> getList(PageParm parm){
        IPage<SysUser> list = sysUserService.list(parm);
        //密码不显示
        list.getRecords().stream().forEach(item ->{
            item.setPassword("");
        });
        // 数据封装
        ResultPageDto<SysUser> pageDto = new ResultPageDto<>();
        pageDto.setData(list.getRecords());
        pageDto.setTotal((int)list.getTotal());

        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功!",pageDto);
    }

    /**
     * 审核管理员账号
     * @param status
     * @param id
     * @return
     */
    @PutMapping("/check/{status}")
    @ApiOperation("审核管理员账号")
    public ResultWeb<String> checkSysUser(@PathVariable Integer status,Long id){
        SysUser sysUser =SysUser.builder()
                .status(status)
                .userId(id)
                .build();
        sysUserService.updateById(sysUser);
        return new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"审核通过!");
    }
}

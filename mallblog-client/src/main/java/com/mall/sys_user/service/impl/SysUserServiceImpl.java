package com.mall.sys_user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.sys_user.mapper.SysUserMapper;
import com.mall.sys_user.pojo.PageParm;
import com.mall.sys_user.pojo.SysUser;
import com.mall.sys_user.service.SysUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Override
    public IPage<SysUser> list(PageParm parm) {
        //构造分页对象
        IPage<SysUser> page = new Page<>();
        page.setSize(parm.getPageSize());
        page.setCurrent(parm.getPageNum());
        //构造查询条件
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        //queryWrapper.ne(SysUser::getStatus,1);
        if(StringUtils.isNotEmpty(parm.getNickName())){
            queryWrapper.like(parm.getNickName()!= null,SysUser::getNickName,parm.getNickName());
        }
        return this.baseMapper.selectPage(page,queryWrapper);
    }

}

package com.mall.doc;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.common_user.service.ComUserService;
import com.mall.exception.service.ServiceException;
import com.mall.exception.service.ex.FileSizeLimitException;
import com.mall.utills.MyUUID;
import com.mall.utills.ValidateCodeUtills;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/doc")
public class DocController {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ComUserService comUserService;

    // 文件存储在本地的路径
    private String path = "E:\\ideaProject\\Mallblog\\photo\\";
//    private String path = "/usr/local/mallblog/photo";

    /**
     * 上传文件到指定的文件夹下
     * @param file
     * @return
     */
    @PostMapping("/upload/{id}")
    public ResultWeb<String> upload(MultipartFile file, @PathVariable Long id){
        // 前端转递一个临时文件，当前代码实现将临死文件保存在指定的文件夹下

        // 1.获取转递的临时文件名
        String originalFilename = file.getOriginalFilename();
        // 2. 获取图片的后缀，从最后的 . 开始截取字符串
        String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
        // 3. 生成新的文件名，防止重复
        String filename = UUID.randomUUID().toString() + substring;
        // 4. 创建目录对象
        File dir = new File(path);
        if (!dir.exists()){
            // 如果文件夹不存在的话，创建
            dir.mkdirs();
        }

        // maxSize 1048576 bytes
        long size = file.getSize();
        if (size > 1048576){
            throw new FileSizeLimitException("文件大小不能超过1M");
        }

        // 5.将文件保存在创建的文件夹下
        try{
            file.transferTo(new File(path + filename));
            System.out.println(path + filename);
        }catch (IOException e){
            e.printStackTrace();
        }
        comUserService.uploadPhoto(filename,id);
        // 上传成功后需要返回，文件的名称，后续存储商品信息时使用
        return new ResultWeb<>(ResultCode.INSERT_SUCCESS,"文件插入成功",filename);
    }

    /**
     * 加载文件到网页
     * @param name
     * @param response
     * @return
     */
    @GetMapping("/download")
    public ResultWeb<String> download(String name, HttpServletResponse response){

        try {
            // 通过输入流读取文件内容
            FileInputStream fileInputStream = new FileInputStream(new File(path + name));

            // 通过输出流将文件写回浏览器
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("image/jpeg");

            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }

            // 关闭资源
            outputStream.close();
            fileInputStream.close();
        }catch (Exception exception){
            exception.printStackTrace();
        }


        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"加载成功");
    }

    @GetMapping("/code")
    public void getCode(HttpServletRequest request, HttpServletResponse response){

        // 创建session
        HttpSession session = request.getSession();
        // 删除原有的session
        request.getServletContext().removeAttribute(ResultCode.VALIDATECODE);
        session.removeAttribute(ResultCode.VALIDATECODE);

        ValidateCodeUtills codeUtills = new ValidateCodeUtills();
        BufferedImage image = codeUtills.getRandCode();
        // 将生成的字符串存储到session中
        session.setAttribute(ResultCode.VALIDATECODE,codeUtills.getStrCode());
        // ServletContext对象全局唯一，不同的controller之间可以相互访问
        request.getServletContext().setAttribute(ResultCode.VALIDATECODE,codeUtills.getStrCode());

        // 将生成的验证码存储在redis中，使用随机生成的字符串作为键值key，并最终返回给前端
        String key = MyUUID.getGuid();
        Cookie cookie = new Cookie("codeKey", key);
        cookie.setMaxAge(60);   // 设置cookie的失效时间为一分钟

        // 路径最好设置成“/”，前端读取cookie时，是无法读取到不同路径下的cookie
        cookie.setPath("/");
        cookie.setHttpOnly(false);
//        cookie.setSecure(false);
        // 同时设置存储到redis中，并设置过期时长为一分钟
        redisTemplate.opsForValue().set(key,codeUtills.getStrCode(),60, TimeUnit.SECONDS);

        // 将BufferedImage转换成byte
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(image,"jpeg",out);
            byte[] bytes = out.toByteArray();
            ServletOutputStream outputStream = response.getOutputStream();


            response.setContentType("image/jpeg");
            response.addCookie(cookie);

            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

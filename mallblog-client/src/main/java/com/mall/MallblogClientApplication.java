package com.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

@EnableFeignClients
@SpringBootApplication
public class MallblogClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallblogClientApplication.class, args);
    }

    @Configuration
    public class IndexConfig {
        @EventListener({ApplicationReadyEvent.class})
        void applicationRead(ApplicationReadyEvent event) {
            Environment env = event.getApplicationContext().getEnvironment();

            String port = env.getProperty("server.port");
            //String path = env.getProperty("server.servlet.context-path");

            String apiDcoUrl = "http://localhost"+ ":" + port + "/doc.html";
            System.out.println("\n----------------------------------------------------------\n\t" +
                    "api-doc: \thttp://localhost" + ":" + port + "/doc.html\n\t" +
                    "----------------------------------------------------------");
            System.out.println("应用已准备就绪... 启动apidoc---查看接口文档");
        }
    }

}

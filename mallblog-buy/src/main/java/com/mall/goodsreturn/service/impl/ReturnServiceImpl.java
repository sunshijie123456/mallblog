package com.mall.goodsreturn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mall.exception.service.ex.NullKeywodsException;
import com.mall.goodsreturn.dto.PageDto;
import com.mall.goodsreturn.mapper.ReturnMapper;
import com.mall.goodsreturn.pojo.PageParameter;
import com.mall.goodsreturn.pojo.ReturnOrder;
import com.mall.goodsreturn.service.ReturnService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReturnServiceImpl extends ServiceImpl<ReturnMapper, ReturnOrder> implements ReturnService {

    /**
     * 用户退货申请实现
     * @param returnOrder
     * @return
     */
    @Override
    public Boolean createApply(ReturnOrder returnOrder) {

        boolean flag = this.save(returnOrder);

        return flag;
    }

    /**
     * 分页查询商品的退货申请
     * @param parameter
     * @return
     */
    @Override
    public PageDto<ReturnOrder> getPage(PageParameter parameter) {

        Integer pageNum = parameter.getPageNum();
        Integer pageSize = parameter.getPageSize();

        if (pageNum == null || pageSize == null){
            throw new NullKeywodsException("当前页和分页大小不能为空");
        }

        String commodityName = parameter.getCommodityName();
        Long consumerId = parameter.getConsumerId();
        Long sellerId = parameter.getSellerId();
        String consumerName = parameter.getConsumerName();
        String sellerName = parameter.getSellerName();
        Integer applyStatus = parameter.getApplyStatus();

        LambdaQueryWrapper<ReturnOrder> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.like(!"".equals(commodityName) && commodityName.length() > 0, ReturnOrder::getCommodityName,commodityName)
                .eq(consumerId != null,ReturnOrder::getConsumerId,consumerId)
                .eq(sellerId != null,ReturnOrder::getSellerId,sellerId)
                .eq(applyStatus != null,ReturnOrder::getApplyStatus,applyStatus)
                .like(!"".equals(consumerName) && consumerName.length() > 0, ReturnOrder::getConsumerName,consumerName)
                .like(!"".equals(sellerName) && sellerName.length() > 0, ReturnOrder::getSellerName,sellerName);

        Page<ReturnOrder> page = new Page<>(pageNum,pageSize);

        Page<ReturnOrder> orderPage = this.page(page, queryWrapper);
        List<ReturnOrder> records = orderPage.getRecords();
        PageDto<ReturnOrder> pageDto = new PageDto<>();
        pageDto.setData(records);
        pageDto.setTotal(orderPage.getTotal());

        return pageDto;
    }

    /**
     * 更新退货申请的状态
     * @param updateflag
     * @param status
     * @param id
     * @return
     */
    @Override
    public Boolean updateStatus(Integer updateflag, Integer status, Long id) {

        if (updateflag == null || status == null || id == null){
            throw new NullKeywodsException("更新申请状态关键字不能为空");
        }

        LambdaUpdateWrapper<ReturnOrder> updateWrapper = new LambdaUpdateWrapper<>();

        if (updateflag == 1){
            updateWrapper.set(ReturnOrder::getAdminStatus,status);
        }else {
            updateWrapper.set(ReturnOrder::getApplyStatus,status);
        }
        updateWrapper.eq(ReturnOrder::getId,id);

        return this.update(updateWrapper);
    }
}

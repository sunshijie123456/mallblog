package com.mall.goodsreturn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.goodsreturn.dto.PageDto;
import com.mall.goodsreturn.pojo.PageParameter;
import com.mall.goodsreturn.pojo.ReturnOrder;

public interface ReturnService extends IService<ReturnOrder> {

    /**
     * 创建用户退货申请
     * @param returnOrder
     * @return
     */
    Boolean createApply(ReturnOrder returnOrder);

    /**
     * 分页查询商品的退货申请
     * @param parameter
     * @return
     */
    PageDto<ReturnOrder> getPage(PageParameter parameter);

    /**
     * 更新申请状态
     * updateflag:表示更新的字段，1-管理，0-用户
     * @param updateflag
     * @param status
     * @param id
     * @return
     */
    Boolean updateStatus(Integer updateflag, Integer status,Long id);
}

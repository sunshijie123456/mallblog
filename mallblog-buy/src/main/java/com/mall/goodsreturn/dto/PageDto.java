package com.mall.goodsreturn.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("分页查询结果")
public class PageDto <T>{

    private Long total;

    private List<T> data;
}

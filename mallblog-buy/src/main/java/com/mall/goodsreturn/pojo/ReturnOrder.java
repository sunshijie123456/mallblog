package com.mall.goodsreturn.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "商品退货申请信息")
@TableName(value = "by_return_product")
public class ReturnOrder {

    private Long id;
    private Long commodityId;
    private String commodityName;
    private BigDecimal commodityPrice;
    private Integer commodityNumber;
    private BigDecimal gross;
    private Long consumerId;
    private String consumerName;
    private Long sellerId;
    private String sellerName;
    private String result;
    private String image;
    private Integer flag;
    private Integer applyStatus;
    private Integer adminStatus;

    // 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)    // 相关处理见  /common/MyMetaObjectHandler.class
    private LocalDateTime createTime;

    // 更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    // 发布人
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    // 更新人
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;
}

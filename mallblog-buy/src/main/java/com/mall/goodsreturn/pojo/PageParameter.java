package com.mall.goodsreturn.pojo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "分页查询参数")
public class PageParameter {

    private Long consumerId;
    private Long sellerId;
    private String commodityName;
    private String consumerName;
    private String sellerName;
    private Integer applyStatus;

    private Integer pageNum;
    private Integer pageSize;

}

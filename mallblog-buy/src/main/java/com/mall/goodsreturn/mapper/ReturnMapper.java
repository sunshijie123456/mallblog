package com.mall.goodsreturn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.goodsreturn.pojo.ReturnOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ReturnMapper extends BaseMapper<ReturnOrder> {
}

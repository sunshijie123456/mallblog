package com.mall.goodsreturn.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.goodsreturn.dto.PageDto;
import com.mall.goodsreturn.pojo.PageParameter;
import com.mall.goodsreturn.pojo.ReturnOrder;
import com.mall.goodsreturn.service.ReturnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/return")
@Api(tags = "商品退货申请接口")
public class ReturnController {

    @Autowired
    private ReturnService returnService;

    @PostMapping("/create")
    @ApiOperation(value = "创建退货申请")
    public ResultWeb<String> createApply(@RequestBody ReturnOrder returnOrder){

        Boolean flag = returnService.createApply(returnOrder);

        return flag ? new ResultWeb<>(ResultCode.INSERT_SUCCESS,"退货申请创建成功")
                : new ResultWeb<>(ResultCode.INSERT_ERROR,"退货申请创建失败");
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询退货申请信息")
    public ResultWeb<PageDto<ReturnOrder>> getPage(@RequestBody PageParameter parameter){

        PageDto<ReturnOrder> data = returnService.getPage(parameter);

        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"分页查询退货申请成功",data);
    }

    @GetMapping("/update")
    @ApiOperation(value = "更新申请状态")
    public ResultWeb<String> updateStatus(@RequestParam Integer updateflag, Integer status, Long id){

        Boolean flag = returnService.updateStatus(updateflag,status,id);

        if (updateflag == 1){
            return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"更新管理员审核状态成功")
                    : new ResultWeb<>(ResultCode.UPDATE_ERROR,"更新管理员审核状态失败");
        }

        return flag ? new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"更新用户确认状态成功")
                : new ResultWeb<>(ResultCode.UPDATE_ERROR,"更新用户确认状态失败");
    }
}

package com.mall.goods.service;

import com.mall.goods.pojo.Goods;
import com.mall.goods.pojo.GoodsDto;
import com.mall.goods.pojo.OrderPageQueryDTO;
import com.mall.goods.tools.ResultPageDto;

import java.util.List;

public interface GoodsOrderService {
    /**
     * 商品订单分页查询
     * @param orderPageQueryDTO
     * @return
     */
   ResultPageDto pageQuery(OrderPageQueryDTO orderPageQueryDTO);

    /**
     * 商品购买
     * @param goodsDto
     */
    void buy(GoodsDto goodsDto);

    /**
     * 订单状态修改
     * @param orderStatus
     * @param id
     */
    void orderStatusChange(Integer orderStatus, Long id);

    /**
     * 获取用户的商品订单信息
     * @param id
     * @return
     */
    List<Goods> getPersoanlCommodity(Long id);
}

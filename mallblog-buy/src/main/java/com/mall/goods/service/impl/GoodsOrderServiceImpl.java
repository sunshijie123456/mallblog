package com.mall.goods.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mall.exception.service.ex.NullKeywodsException;
import com.mall.goods.mapper.GoodsOrderMapper;
import com.mall.goods.pojo.Goods;
import com.mall.goods.pojo.GoodsDto;
import com.mall.goods.pojo.OrderPageQueryDTO;
import com.mall.goods.service.GoodsOrderService;
import com.mall.goods.tools.ResultPageDto;
import com.mall.goods.tools.orderNoCreate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GoodsOrderServiceImpl implements GoodsOrderService {
    @Autowired
    private GoodsOrderMapper goodsOrderMapper;

    /**
     * 商品订单分页查询
     * @param orderPageQueryDTO
     * @return
     */
    public ResultPageDto pageQuery(OrderPageQueryDTO orderPageQueryDTO) {
        //开始分页查询
        PageHelper.startPage(orderPageQueryDTO.getPage(), orderPageQueryDTO.getPageSize());
        Page<Goods> page = goodsOrderMapper.pageQuery(orderPageQueryDTO);
        long total = page.getTotal();
        List<Goods> result = page.getResult();
        return new ResultPageDto(total, result);
    }

    /**
     * 商品购买
     * @param goodsDto
     */
    public void buy(GoodsDto goodsDto) {

        // 判断用户的个人信息是否全面
        if (goodsDto.getClientPhone() == null || goodsDto.getAddress() == null){
            throw new NullKeywodsException("手机号地址不能为空，请完善个人信息之后购买商品");
        }
        Goods goods = new Goods();
        
        BeanUtils.copyProperties(goodsDto,goods);

        //设置账号状态，默认为审核状态
        goods.setOrderStatus(0);

        //设置订单创建时间
        goods.setCreateTime(LocalDateTime.now());

        //设置订单编号
        goods.setOrderId(orderNoCreate.randomOrderCode());
        
        goodsOrderMapper.insert(goods);
    }

    /**
     * 订单状态修改
     * @param orderStatus
     * @param id
     */
    public void orderStatusChange(Integer orderStatus, Long id) {
        Goods goods = Goods.builder()
                .orderStatus(orderStatus)
                .id(id)
                .build();
        goodsOrderMapper.update(goods);
    }

    /**
     * 获取用户个人的商品订单信息
     * @param id
     * @return
     */
    @Override
    public List<Goods> getPersoanlCommodity(Long id) {

        return goodsOrderMapper.getProductOrderById(id);
    }
}

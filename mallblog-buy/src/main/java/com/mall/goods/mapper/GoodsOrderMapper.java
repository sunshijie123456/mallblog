package com.mall.goods.mapper;

import com.github.pagehelper.Page;
import com.mall.goods.pojo.Goods;
import com.mall.goods.pojo.OrderPageQueryDTO;
import com.mall.member.pojo.Member;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface GoodsOrderMapper {
    /**
     * 分页查询
     * @param orderPageQueryDTO
     * @return
     */
    Page<Goods> pageQuery(OrderPageQueryDTO orderPageQueryDTO);

    /**
     * 购买商品
     * @param goods
     */
    @Insert("insert into goods_order(order_id, goods_name, goods_price, client_name, client_phone, address, order_status, create_user, create_time)" +
            "values " +
            "(#{orderId},#{goodsName},#{goodsPrice},#{clientName},#{clientPhone},#{address},#{orderStatus},#{createUser},#{createTime})")
    void insert(Goods goods);

    /**
     * 订单状态修改
     * @param goods
     */
    @Update("update goods_order set order_status = #{orderStatus} where id = #{id}")
    void update(Goods goods);

    /**
     * 更具用户id查询商品订单信息
     * @param id
     * @return
     */
    @Select("select * from goods_order where create_user = #{id};")
    List<Goods> getProductOrderById(Long id);
}

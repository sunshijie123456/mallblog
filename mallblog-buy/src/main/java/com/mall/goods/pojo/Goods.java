package com.mall.goods.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 商品订单
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Goods implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    //订单编号
    private String orderId;

    //商品名称
    private String goodsName;

    //订单金额
    private Double goodsPrice;

    //收货人姓名
    private String clientName;
    
    //收货人电话
    private String clientPhone;

    //收货人地址
    private String address;

    //订单状态，0-申请中，1-订单完成，2-退货中
    private Integer orderStatus;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    private Long createUser;

    private String goodsImage;
}

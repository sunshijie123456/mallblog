package com.mall.goods.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderPageQueryDTO implements Serializable {

    //订单编号
    private String orderId;
    
    //买家id
    private Long createUser;

    //订单状态，0-申请中，1-订单完成，2-退货中
    private Integer orderStatus;

    //页码
    private int page;

    //每页显示记录数
    private int pageSize;

}

package com.mall.goods.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
 * 购买商品接口字段
 */
@Data
public class GoodsDto implements Serializable {
    private Long id;

    //商品名称
    private String goodsName;

    //订单金额
    private Double goodsPrice;

    //收货人姓名
    private String clientName;

    //收货人电话
    private String clientPhone;

    //收货人地址
    private String address;

    private Long createUser;
}

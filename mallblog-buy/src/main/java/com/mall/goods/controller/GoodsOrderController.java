package com.mall.goods.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.goods.pojo.Goods;
import com.mall.goods.pojo.GoodsDto;
import com.mall.goods.pojo.OrderPageQueryDTO;
import com.mall.goods.service.GoodsOrderService;
import com.mall.member.pojo.MemberPageQueryDTO;
import com.mall.goods.tools.ResultPageDto;
import com.mall.publisher.RabbitMqPublisher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品订单相关接口
 */
@RestController
@RequestMapping("/order")
@Api(tags = "商品订单相关接口")
public class GoodsOrderController {
    @Autowired
    private GoodsOrderService goodsOrderService;

    /**
     * 商品购买
     * @param goodsDto
     * @return
     */
    @PostMapping("/buy")
    @ApiOperation("商品购买")
    public ResultWeb<String> buyGoods(@RequestBody GoodsDto goodsDto){
        goodsOrderService.buy(goodsDto);
        return new ResultWeb<>(ResultCode.INSERT_SUCCESS,"商品购买成功!");
    }

    /**
     * 商品订单分页查询
     * @param orderPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询")
    public ResultWeb<ResultPageDto> page(OrderPageQueryDTO orderPageQueryDTO){
        ResultPageDto pageDto = goodsOrderService.pageQuery(orderPageQueryDTO);
        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功!",pageDto);
    }

    /**
     * 订单状态修改
     * @param orderStatus
     * @param id
     * @return
     */
    @PostMapping("/orderStatusChange/{orderStatus}")
    @ApiOperation("订单状态修改")
    public ResultWeb<String> orderStatusChange(@PathVariable Integer orderStatus, Long id){
        goodsOrderService.orderStatusChange(orderStatus,id);
        if(orderStatus == 2){
            return new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"订单退货成功!");
        }
        return new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"订单完成!");
    }

    @GetMapping("/product/{id}")
    @ApiOperation("获取个人商品订单信息")
    public ResultWeb<List<Goods>> getPersoanlCommodity(@PathVariable Long id){
        List<Goods> list = goodsOrderService.getPersoanlCommodity(id);

        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功!",list);
    }
}

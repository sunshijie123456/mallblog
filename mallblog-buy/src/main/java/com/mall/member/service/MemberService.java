package com.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mall.member.pojo.Member;
import com.mall.member.pojo.MemberDto;
import com.mall.member.pojo.MemberPageQueryDTO;
import com.mall.member.tools.ResultPageDto;

import java.util.List;

public interface MemberService {
    /**
     * 注册会员
     * @param memberDto
     */
    void registeredMember(MemberDto memberDto);

    /**
     * 会员订单分页查询
     * @param memberPageQueryDTO
     * @return
     */
    ResultPageDto pageQuery(MemberPageQueryDTO memberPageQueryDTO);

    /**
     * 会员资格审查
     * @param orderStatus
     * @param id
     */
    void checkMember(Integer orderStatus, Long id, Long updateUser, Long createUser);

    /**
     * 获取个人购买会员订单信息
     * @param id
     * @return
     */
    List<Member> getPersoanlMember(Long id);
}

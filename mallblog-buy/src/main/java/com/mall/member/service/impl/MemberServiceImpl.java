package com.mall.member.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mall.member.mapper.MemberMapper;
import com.mall.member.pojo.Member;
import com.mall.member.pojo.MemberDto;
import com.mall.member.pojo.MemberPageQueryDTO;
import com.mall.member.service.MemberService;
import com.mall.member.tools.ResultPageDto;
import com.mall.member.tools.orderNoCreate;
import com.mall.publisher.RabbitMqPublisher;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private RabbitMqPublisher rabbitMqPublisher;

    /**
     * 注册会员
     *
     * @param memberDto
     */
    public void registeredMember(MemberDto memberDto) {
        Member member = new Member();

        //对象属性拷贝
        BeanUtils.copyProperties(memberDto, member);

        //设置账号状态，默认为审核状态
        member.setOrderStatus(0);

        //设置订单创建时间
        member.setCreateTime(LocalDateTime.now());
        // 添加延迟消息到队列，监听用户会员状态
        rabbitMqPublisher.setMemberDeadLine(memberDto.getCreateUser(),memberDto.getMemberType());

        //设置订单编号
        member.setOrderId(orderNoCreate.randomOrderCode());

        memberMapper.insert(member);
    }

    /**
     * 会员订单分页查询
     *
     * @param memberPageQueryDTO
     * @return
     */
    public ResultPageDto pageQuery(MemberPageQueryDTO memberPageQueryDTO) {
        //开始分页查询
        PageHelper.startPage(memberPageQueryDTO.getPage(), memberPageQueryDTO.getPageSize());
        Page<Member> page = memberMapper.pageQuery(memberPageQueryDTO);
        long total = page.getTotal();
        List<Member> result = page.getResult();
        return new ResultPageDto(total, result);
    }

    /**
     * 会员资格审查
     *
     * @param orderStatus
     * @param id
     */
    public void checkMember(Integer orderStatus, Long id, Long updateUser, Long createUser) {
        LocalDateTime memberDay = LocalDateTime.now();

        //会员到期时间计算
        Member entity = memberMapper.selectById(createUser);
        Integer memberType = entity.getMemberType();
        
        if (memberType == 0) {
            memberDay = memberDay.plusMonths(1).toInstant(ZoneOffset.UTC).atZone(ZoneId.systemDefault()).toLocalDateTime();
        } else if (memberType == 1) {
            memberDay = memberDay.plusMonths(3).toInstant(ZoneOffset.UTC).atZone(ZoneId.systemDefault()).toLocalDateTime();
        } else if (memberType == 2) {
            memberDay = memberDay.plusYears(1).toInstant(ZoneOffset.UTC).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }

        Member member = Member.builder()
                .orderStatus(orderStatus)
                .id(id)
                .memberDay(memberDay)
                .updateUser(updateUser)
                .updateTime(LocalDateTime.now())
                .build();
        memberMapper.update(member);
    }

    /**
     * 获取个人购买会员订单信息
     * @param id
     * @return
     */
    @Override
    public List<Member> getPersoanlMember(Long id) {


        return memberMapper.getMemberById(id);
    }
}

package com.mall.member.mapper;

import com.github.pagehelper.Page;
import com.mall.member.pojo.Member;
import com.mall.member.pojo.MemberPageQueryDTO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface MemberMapper {
    /**
     * 会员注册
     * @param member
     */
    @Insert("insert into bu_member(order_id, member_type, member_day, gross, order_status, create_user, create_time, update_user, update_time)" +
            "values " +
            "(#{orderId},#{memberType},#{memberDay},#{gross},#{orderStatus},#{createUser},#{createTime},#{updateUser},#{updateTime})")
    void insert(Member member);

    /**
     * 分页查询
     * @param memberPageQueryDTO
     * @return
     */
    Page<Member> pageQuery(MemberPageQueryDTO memberPageQueryDTO);

    /**
     * 根据主键动态修改属性
     * @param member
     */
    void update(Member member);

    /**
     * 根据id查询会员信息
     * @param createUser
     * @return
     */
    Member selectById(Long createUser);

    /**
     * 通过id获取用户的会员订单信息
     * @param id
     * @return
     */
    @Select("select * from bu_member where create_user = #{id};")
    List<Member> getMemberById(Long id);
}

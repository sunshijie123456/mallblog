package com.mall.member.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MemberDto implements Serializable {
    
    private Long id;

    //会员类型
    private Integer memberType;

    //订单总额
    private double gross;

    //订单创建人
    private Long createUser;
}

package com.mall.member.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MemberPageQueryDTO implements Serializable {

    //订单编号
    private String orderId;

    //页码
    private int page;

    //每页显示记录数
    private int pageSize;

}

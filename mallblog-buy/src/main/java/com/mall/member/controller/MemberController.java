package com.mall.member.controller;

import com.mall.common.ResultCode;
import com.mall.common.ResultWeb;
import com.mall.goods.pojo.Goods;
import com.mall.member.pojo.Member;
import com.mall.member.pojo.MemberDto;
import com.mall.member.pojo.MemberPageQueryDTO;
import com.mall.member.service.MemberService;
import com.mall.member.tools.ResultPageDto;
import com.mall.publisher.RabbitMqPublisher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/member")
@Api(tags = "会员管理操作接口")
public class MemberController {
    @Autowired
    private MemberService memberService;

    /**
     * 注册会员
     * @param memberDto
     * @return
     */
    @PostMapping("/register")
    @ApiOperation("注册会员")
    public ResultWeb<String> registeredMember(@RequestBody MemberDto memberDto){
        memberService.registeredMember(memberDto);
        return new ResultWeb<>(ResultCode.INSERT_SUCCESS, "会员申请成功!");
    }

    /**
     * 会员订单分页查询
     * @param memberPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询")
    public ResultWeb<ResultPageDto> page(MemberPageQueryDTO memberPageQueryDTO){
        ResultPageDto pageDto = memberService.pageQuery(memberPageQueryDTO);
        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功!",pageDto);
    }

    /**
     * 会员资格审查
     * @param orderStatus
     * @param id
     * @return
     */
    @PostMapping("/checkMember/{orderStatus}")
    @ApiOperation("会员资格审查")
    public ResultWeb<String> checkMember(@PathVariable Integer orderStatus, Long id, Long updateUser, Long createUser){
        memberService.checkMember(orderStatus,id,updateUser,createUser);
        return new ResultWeb<>(ResultCode.UPDATE_SUCCESS,"会员资格审查通过!");
    }

    /**
     * 获取个人购买会员订单信息
     * @param id
     * @return
     */
    @GetMapping("/order/{id}")
    @ApiOperation("获取个人购买会员订单信息")
    public ResultWeb<List<Member>> getPersoanlMember(@PathVariable Long id){
        List<Member> list = memberService.getPersoanlMember(id);

        return new ResultWeb<>(ResultCode.QUERY_SUCCESS,"查询成功!",list);
    }
}

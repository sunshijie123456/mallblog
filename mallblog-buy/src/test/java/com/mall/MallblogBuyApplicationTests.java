package com.mall;

import com.mall.publisher.RabbitMqPublisher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class MallblogBuyApplicationTests {

    @Autowired
    private RabbitMqPublisher rabbitMqPublisher;

    @Test
    void contextLoads() {
    }

    @Test
    void TextSendMemberDeadLine() {
        rabbitMqPublisher.setMemberDeadLine(16L,0);
        System.out.println(LocalDateTime.now());
    }
}
